jQuery(document).ready(function($) {
	var t = $('table').not('.no-footable');
	t.find('th:not(:nth-child(2))').not('[data-hide]').attr('data-hide', 'phone');
	t.footable();
});