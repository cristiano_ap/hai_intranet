(function() {
    $(document).ajaxError(function(event, xhr) {
        if (xhr.status == 403) {
            $(window).off('beforeunload');
            $('body').removeClass('form-unsaved');
            location.reload();
        }
    });
})();