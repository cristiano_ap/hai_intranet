/*Client ajax*/
$(document).ready(function() {
	$('#form-order-add').on('change', '#client-id', function(e) {
        var form = $(e.delegateTarget);
		var client_id = $(this).val();
        if(!client_id){
            return;
        }
	    $.ajax({
	    	url: App.base + '/clients/view/' + client_id + '.json',
	    	timeout: 5000
	    })
	    .done(function(data) {
	    	var client = data.client;
	    	$(form).find('#client-individual-registration').val(client.individual_registration_formatted);
	    	$(form).find('#client-state-registration').val(client.state_registration_formatted);
	    	$(form).find('#client-address').val(client.address);
	    	$(form).find('#client-district').val(client.district);
	    	$(form).find('#client-phone1').val(client.phone1);
	    	$(form).find('#client-zipcode').val(client.zipcode);
	    	$(form).find('#client-city').val(client.city);
	    	$(form).find('#client-state').val(client.state);
	    })
	    .always(function() {
            console.log("client load complete");
	    });

	});
    if(!$('#client-id').val() && $_GET['client_id'] !== undefined) {
        $('#client-id').val($_GET['client_id']);
    }
    $('#client-id').trigger('change').trigger('chosen:updated');
    if($_GET['from_quote'] !== undefined) {
        $('#client-id').prop('disabled', true).trigger("chosen:updated").prop('disabled', false);
    }
});
/*Products table*/
$(document).ready(function() {
	if(!$('body.Order.add')){
		return;
	}
	// load a language
    numeral.language('br', {
        delimiters: {
            thousands: ' ',
            decimal: '.'
        },
        abbreviations: {
            thousand: 'K',
            million: 'M',
            billion: 'B',
            trillion: 'T'
        },
        // ordinal : function (number) {
        //     return number === 1 ? 'er' : 'ème';
        // },
        currency: {
            symbol: 'R$'
        }
    });

// switch between languages
    numeral.language('br');
    $form       = $('#dynamic').calx({
    	onAfterRender:function(){
            var final_price = $('#final_price').val();
            $('#final_price').val(final_price.replace(',', '.')).trigger('change');

            var refresh = function(e) {
                var data={};
                var i=0;
                $('#dynamic #itemlist').find('tr').each(function(){
                    var row={};
                    $(this).find('input,select,textarea').each(function() {
                        if(!this.name) return;
                        row[$(this).attr('name')]=$(this).val();
                    });
                    data[i++] = row;
                });
                var serialized = JSON.stringify(data);
                $('#form-order-add').find('#products-data').val(serialized);
            }
            refresh();
        }
    });
    $itemlist   = $('#itemlist');
    $counter    = typeof $counter == 'undefined' ? 0 : $counter;

    $('#add_item').click(function(e){
        e.preventDefault();
        var i = ++$counter;
        $itemlist.append(
            '<tr>\
                <td colspan="2"><input class="form-control input-sm text-right product-id" data-cell="A'+i+'" name="products['+i+'][A'+i+']" data-format="0" type="hidden">\
                <input class="form-control input-sm product-name" data-cell="B'+i+'" name="products['+i+'][B'+i+']" required></td>\
                <td><input class="form-control input-sm text-right" data-cell="C'+i+'" name="products['+i+'][C'+i+']" required data-format="0[.]00"></td>\
                <td><input class="form-control input-sm text-right" data-cell="D'+i+'" name="products['+i+'][D'+i+']" required data-format="$ 0,0.00"></td>\
                <td><input class="form-control input-sm text-right" data-cell="E'+i+'" name="products['+i+'][E'+i+']" data-format="0,0[.]00 %"></td>\
                <td><input class="form-control input-sm text-right" data-cell="F'+i+'" name="products['+i+'][F'+i+']" required data-format="$ 0,0.00" data-formula="D'+i+'*(C'+i+'-(C'+i+'*E'+i+'))"></td>\
                <td class="text-center"><button class="btn-remove btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></button></td>\
            </tr>'
        );
        //console.log('new row appended');

        $form.calx('update');
        $form.calx('getCell', 'G1').setFormula('SUM(F1:F'+i+')');

        //console.log($form.calx('getSheet'));
    });
	if ($counter == 0) {
		$('#add_item').trigger('click');
	};

    $('#itemlist').on('click', '.btn-remove', function(){
        $(this).parent().parent().remove();
        --$counter;
        $form.calx('update');
        $form.calx('getCell', 'G1').calculate();
    });
});
