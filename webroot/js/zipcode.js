$(document).ready(function() {
	$('body.Clients #zipcode').on('change', function(e) {
		var zip = $(this).val().replace('-', '');
	    $('body.Clients').find('#district, #city, #address, #state').attr('disabled', 'disabled');
	    $.ajax({
	    	url: 'https://viacep.com.br/ws/' + zip + '/json/',
	    	timeout: 2500
	    })
	    .done(function(data) {
	    	$('body.Clients #address').val(data.logradouro);
	    	$('body.Clients #district').val(data.bairro);
	    	$('body.Clients #city').val(data.localidade);
	    	$('body.Clients #state').val(data.uf);
	    })
	    .always(function() {
	    	$('body.Clients').find('#district, #city, #address, #state').removeAttr('disabled');
	    	console.log("zipcode search complete");
	    });
	    
	});
});