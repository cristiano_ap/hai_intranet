(function() {
    $(document).ready(function() {
        var $form = $('#content form'),
            origForm = $form.serialize();
        $('#content form :input').on('change input', function() {
            if ($form.serialize() !== origForm) {
                $('body').addClass('form-unsaved');
            }
        });
        window.onbeforeunload = function(e) {
            /*$('.change-message').toggle($form.serialize() !== origForm);*/
            if ($('body.form-unsaved').length) {
                return "Existem dados não salvos, tem certeza?";
            }
        }
    });
    $(document).ready(function() {
        var target = 'button[type=submit], input[type=submit]';
        $(target).attr('data-style', 'expand-right').addClass('btn btn-primary ladda-button');
        $('form').find(target).on('click', function(e) {
            var form = $(this).parents('form')[0];
            if (form.checkValidity() == true) {
                var l = Ladda.create(this);
                l.start();
                $(this).attr('disabled', 'disabled');
                $('[type=button], button').attr('disabled', 'disabled');
                $('body').removeClass('form-unsaved');
                form.submit();
            }
        });
    });
    $(document).ready(function() {
        $('select').not('.no-chosen').chosen({
            no_results_text: "=("
        })
        .each(function() {
            //    take each select and put it as a child of the chosen container
            //    this mean it'll position any validation messages correctly
            $(this).next(".chosen-container").prepend($(this).detach());

            //    apply all the styles, personally, I've added this to my stylesheet
            $(this).attr("style","display:block!important; position:absolute; clip:rect(0,0,0,0)");

            //    to all of these events, trigger the chosen to open and receive focus
            $(this).on("click focus keyup",function(event){
                $(this).closest(".chosen-container").trigger("mousedown.chosen");
            });
        });
        $('div.form-group').not('.textarea').matchHeight();
    });
    $(document).ready(function() {
        enquire.register("screen and (max-width:767px)", {
            match: function() {
                $('.sidebar .actions').empty();
            },
        });
        enquire.register("screen and (min-width:768px)", {
            match: function() {
                $('[role=navigation] .contextual').empty();
            },
        });
    });
})();
