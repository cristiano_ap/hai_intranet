(function ($) {
    $(document).ready(function () {
        $('.cep').mask('00000-000');
        $('#phone1,#phone2').mask('(00) 0000-0000');
        $('#zipcode').mask('00000-000');
        $('#individual-registration')
            .on('blur', function () {
                var SPMaskBehavior = function (val) {
                    return val.replace(/[\.-]/g, '').length <= 11 ? '000.000.000-00' : 'Z00.000.000/0000-00';
                };
                var spOptions = {
                    reverse: true,
                    translation: {
                        'Z': {
                            pattern: /[0-9]/,
                            optional: true
                        }
                    }
                };
                $(this).mask(SPMaskBehavior, spOptions);
            })
            .on('focus', function () {
                $(this).unmask();
            })
            .trigger('blur');
    });
})(jQuery)