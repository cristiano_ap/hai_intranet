<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClientsFixture
 *
 */
class ClientsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'individual_registration' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'state_registration' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'address' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'city' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'district' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'zipcode' => ['type' => 'string', 'length' => 9, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'phone1' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'phone2' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'individual_registration' => ['type' => 'unique', 'columns' => ['individual_registration'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'João',
            'individual_registration' => '088.324.876-08',
            'state_registration' => '',
            'address' => 'Rua A, 84',
            'city' => 'Betim',
            'district' => 'Cruzeiro',
            'zipcode' => '24748-803',
            'phone1' => '32 98248-4894',
            'phone2' => '',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
        [
            'id' => 2,
            'name' => 'Lorem ipsum dolor sit amet',
            'individual_registration' => '038.324.876-08',
            'state_registration' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'district' => 'Lorem ipsum dolor sit amet',
            'zipcode' => 'Lorem i',
            'phone1' => 'Lorem ipsum dolor ',
            'phone2' => 'Lorem ipsum dolor ',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
        [
            'id' => 3,
            'name' => 'Lorem ipsum dolor sit amet',
            'individual_registration' => '201.213.234/0001-99',
            'state_registration' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'district' => 'Lorem ipsum dolor sit amet',
            'zipcode' => 'Lorem i',
            'phone1' => 'Lorem ipsum dolor ',
            'phone2' => 'Lorem ipsum dolor ',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
        [
            'id' => 4,
            'name' => 'Lorem ipsum dolor sit amet',
            'individual_registration' => '201.213.234/0001-44',
            'state_registration' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'district' => 'Lorem ipsum dolor sit amet',
            'zipcode' => 'Lorem i',
            'phone1' => 'Lorem ipsum dolor ',
            'phone2' => 'Lorem ipsum dolor ',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
        [
            'id' => 5,
            'name' => 'Invalid zipcode',
            'individual_registration' => '201.213.234/0001-14',
            'state_registration' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'district' => 'Lorem ipsum dolor sit amet',
            'zipcode' => '314-129',
            'phone1' => 'Lorem ipsum dolor ',
            'phone2' => 'Lorem ipsum dolor ',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
        [
            'id' => 6,
            'name' => 'Valid zipcode',
            'individual_registration' => '201.213.234/0001-33',
            'state_registration' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'district' => 'Lorem ipsum dolor sit amet',
            'zipcode' => '31344-129',
            'phone1' => 'Lorem ipsum dolor ',
            'phone2' => 'Lorem ipsum dolor ',
            'created' => '2016-01-18 01:47:46',
            'modified' => '2016-01-18 01:47:46'
        ],
    ];
}
