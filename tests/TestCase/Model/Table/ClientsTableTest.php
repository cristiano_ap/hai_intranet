<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientsTable Test Case
 */
class ClientsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clients',
        'app.orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clients') ? [] : ['className' => 'App\Model\Table\ClientsTable'];
        $this->Clients = TableRegistry::get('Clients', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clients);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test cpf format method
     *
     * @return void
     */
    public function testFormatCPF()
    {
        $client = $this->Clients->get(1);
        $this->assertEquals($client->isCPF(), true);
    }

    /**
     * Test cnpj format method
     *
     * @return void
     */
    public function testFormatCNPJ()
    {
        $client = $this->Clients->get(3);
        $this->assertEquals($client->isCNPJ(), true);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
