<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $role
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class User extends BaseEntity
{
    protected function _setPassword($password)
    {
        if(strlen($password) > 0){
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    protected function _getRoleFormatted(){
        return array_flip(Configure::read('Roles'))[$this->role];
    }

    protected function _setRole($role){
        $Users = \Cake\ORM\TableRegistry::get('Users');
        if($Users->find()->count()){
            return $role;
        }
        $super = Configure::read('Roles.superadmin');
        return $super;
    }
}
