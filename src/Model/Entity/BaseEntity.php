<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\I18n\Number;

/**
 * Product Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $deleted
 */
class BaseEntity extends Entity
{
    protected $prefix = '';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getIdFormatted(){
        $mininum = 5;
        $size = strlen($this->id) > $mininum ? strlen($this->id) + 1 : $mininum;
        return $this->prefix . str_pad($this->id, $size, '0', STR_PAD_LEFT);
    }

    protected function _getCreatedFormatted(){
        return (new Time($this->created))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }

    protected function _getModifiedFormatted(){
        return (new Time($this->created))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }
}
