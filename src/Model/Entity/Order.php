<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\I18n\Number;

/**
 * Order Entity.
 *
 * @property int $id
 * @property int $client_id
 * @property \App\Model\Entity\Client $client
 * @property float $final_price
 * @property \Cake\I18n\Time $expire_date
 * @property \Cake\I18n\Time $exit_date
 * @property string $location
 * @property string $warranty
 * @property string $data
 * @property \Cake\I18n\Time $paid
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $deleted
 */
class Order extends BaseEntity
{
    protected $prefix = 'ORD-';

    protected function _getFinalPriceFormatted(){
        return Number::currency($this->final_price);
    }

    protected function _getExpireDateFormatted(){
        return (new Time($this->expire_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }

    protected function _getExitDateFormatted(){
        return (new Time($this->exit_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }

    protected function _getPaidFormatted(){
        return (new Time($this->paid))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT]);
    }

    protected function _getDataFormatted(){
        $data = $this->data;
        if($this->hasDiscount()){
            return $data;
        }
        $i = 1;
        foreach ($data as &$row) {
            unset($row["products[$i][E$i]"]);
            $i++;
        }
        return $data;
    }

    public function isPaid(){
        return !is_null($this->paid);
    }

    public function hasDiscount(){
        $discounts = [];

        $i = 1;
        foreach ($this->data as $row) {
            $cell = $row["products[$i][E$i]"];
            $discounts[] = $cell;
            $i++;
        }

        return (bool) array_sum($discounts);
    }

}
