<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $individual_registration
 * @property string $individual_registration_formatted
 * @property string $identifier - virtual
 * @property string $state_registration
 * @property string $address
 * @property string $city
 * @property string $district
 * @property string $state
 * @property string $zipcode
 * @property string $zipcode_formatted
 * @property string $phone1
 * @property string $phone2
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $deleted
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Quote[] $quotes
 */
class Client extends BaseEntity
{
    const FORMAT_CPF_SIZE = 11;
    const FORMAT_CPF = '/[\d]{3}.[\d]{3}.[\d]{3}-[\d]{2}/';
    const FORMAT_CNPJ = '/[\d]{2,3}.[\d]{3}.[\d]{3}\/[\d]{4}-[\d]{2}/';

    protected $prefix = 'CUST-';

    protected $_virtual = [
        'individual_registration_formatted',
        'state_registration_formatted',
        'zipcode_formatted',
    ];

    protected function _getIdentifier(){
        return sprintf("%s - %s (%s)", $this->id_formatted, $this->name, $this->individual_registration_formatted);
    }

    protected function _setIndividualRegistration($individual_registration){
        $individual_registration = ereg_replace('[^0-9]', '', $individual_registration);
        return $individual_registration;
    }

    protected function _getIndividualRegistrationFormatted(){
        if (strlen($this->individual_registration) == self::FORMAT_CPF_SIZE) {
            return preg_replace('/([\d]{3})([\d]{3})([\d]{3})([\d]{2})/', '$1.$2.$3-$4', $this->individual_registration);
        } else {
            return preg_replace('/([\d]{2,3})([\d]{3})([\d]{3})([\d]{4})([\d]{2})/', '$1.$2.$3/$4-$5', $this->individual_registration);
        }
    }

    protected function _getStateRegistrationFormatted(){
        if (empty($this->state_registration)) {
            return '-';
        } else {
            return $this->state_registration;
        }
    }

    protected function _setZipcode($zipcode){
        return ereg_replace('[^0-9]', '', $zipcode);;
    }

    protected function _getZipcodeFormatted(){
        return preg_replace('/([\d]{5})([\d]{3})/', '$1-$2', $this->zipcode);
    }

    public function isCPF(){
        return preg_match(self::FORMAT_CPF, $this->individual_registration_formatted);
    }

    public function isCNPJ(){
        return preg_match(self::FORMAT_CNPJ, $this->individual_registration_formatted);
    }

}
