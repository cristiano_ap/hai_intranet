<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\I18n\Number;

/**
 * Quote Entity.
 *
 * @property int $id
 * @property int $client_id
 * @property \App\Model\Entity\Client $client
 * @property float $final_price
 * @property float $amount_advanced
 * @property float $discount
 * @property \Cake\I18n\Time $expire_date
 * @property \Cake\I18n\Time $exit_date
 * @property string $location
 * @property string $warranty
 * @property string $data
 * @property \Cake\I18n\Time $accepted
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $deleted
 */
class Quote extends BaseEntity
{
    protected $prefix = 'QUO-';

    protected function _getFinalPriceFormatted(){
        return Number::currency($this->final_price);
    }

    protected function _getAmountAdvancedFormatted(){
        return Number::currency($this->amount_advanced);
    }

    protected function _getDiscountFormatted(){
        return Number::toPercentage($this->discount);
    }

    protected function _getExpireDateFormatted(){
        return (new Time($this->expire_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }

    protected function _getExitDateFormatted(){
        return (new Time($this->exit_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
    }

    protected function _getAcceptedFormatted(){
        return (new Time($this->accepted))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT]);
    }

    public function isAccepted(){
        return !is_null($this->accepted);
    }

    public function becameOrder(){
        return !is_null($this->order_id);
    }

    public function hasDiscount(){
        return $this->discount > 0;
    }

}
