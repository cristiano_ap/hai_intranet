<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\I18n\Number;

/**
 * Product Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $sku
 * @property string $unit
 * @property float $price
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $deleted
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Quote[] $quotes
 */
class Product extends BaseEntity
{
    protected $prefix = 'PROD-';

    protected function _getIdentifier(){
        $string = $this->id_formatted;
        if(isset($this->sku)) {
            $string .= sprintf(" - %s", $this->sku);
        }
        $string .= sprintf(" - %s", $this->name);
        if(isset($this->unit)) {
            $string .= sprintf(" (%s)", $this->unit);
        }
        return $string;
    }

    protected function _getPriceFormatted(){
        return Number::currency($this->price);
    }
}
