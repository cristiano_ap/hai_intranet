<?php
namespace App\Model\Table;

use App\Model\Entity\Order;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Chronos\Chronos;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Cake\Database\Schema\Table as Schema;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;
use Cake\Core\Configure;
use Cake\Event\Event;

/**
 * Orders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clients
 */
class OrdersTable extends AppTable
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                ],
            ]
        ]);

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Quotes', [
            // 'conditions' => ['Quote.accepted <> NULL'],
            // 'dependent' => true
        ]);
    }

    /**
     * Initialize schema method
     *
     * @param array $schema The schema for the Table.
     * @return schema
     */
    protected function _initializeSchema(Schema $schema)
    {
        $schema->columnType('data', 'json');
        return $schema;
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
        $validator
            ->add('client_id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('client_id');

        $validator
            ->add('final_price', 'valid', ['rule' => 'numeric'])
            ->requirePresence('final_price', 'create')
            ->notEmpty('final_price');

        $validator
            ->add('expire_date', 'valid', ['rule' => 'date'])
            ->requirePresence('expire_date', 'create')
            ->notEmpty('expire_date')
            ->add('expire_date', 'validDate', [
                'rule' => function ($data, $provider) {
                    $data = Chronos::createFromDate($data['year'], $data['month'], $data['day']);

                    // if (Chronos::now()->lte($data)) {
                    if (!$data->isPast()) {
                       return true;
                    }

                    return __('The expire date cannot be in the past');
                }
            ]);

        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->allowEmpty('warranty');

        $validator
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }

    public function generatePdf($order)
    {
        try {
            $order->client = $this->Clients->get($order->client_id);

            $options = Configure::read('CakePdf') ? : [
                'engine' => 'CakePdf.Mpdf',
                // 'engine' => 'CakePdf.Tcpdf',
                'margin' => [
                    'bottom' => 15,
                    'left' => 15,
                    'right' => 30,
                    'top' => 30
                ],
                'orientation' => 'portrait',
            ];

            $CakePdf = new \CakePdf\Pdf\CakePdf($options);
            $CakePdf->template('order.view', 'default');

            $name = Text::uuid() . ".pdf";
            $path = $url = Configure::read('App.paths.pdfs');
            $path = WWW_ROOT . DS . $path;

            $folder = new Folder($path);
            if($folder->create($path)){
                $folder->chmod($path, 0755, true);
            } else {
                throw new NotFoundException(__('Error on folder creation'));
            }

            $order->pdf_path = str_ireplace('\\', '/', "/$url/$name");
            $order->dirty('pdf_path', true);

            $CakePdf->viewVars([
                'order' => $order
                // 'order' => $order->toArray()
            ]);
            $writed = $CakePdf->write($path . DS . $name);

            if(!$writed) {
                return false;
            }

            $this->save($order);

            $event = new Event('Model.Order.afterGeneratePDF', $this, [
                'order' => $order
            ]);
            $this->eventManager()->dispatch($event);
        } catch (Exception $e) {

        }

        return true;
    }

    /**
     * Update order as paid
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function setAsPaid($order)
    {
        $order->paid = new Time();
        $order->dirty('paid', true);
        return $this->save($order);
    }

    public function getFromQuote($quote_id = null)
    {
        $quote = $this->Quotes->get($quote_id)->toArray();

        for ($i=0; $i < count($quote['data']['items']); $i++) {
            $j = $i + 1;
            $row = @$quote['data']['items'][$i];
            if(!isset($quote['products'])){
                $quote['products'] = [];
            }

            $quote['products'][$j] = [
                "A$j" => $row[ "products[$j][A$j]" ],
                "B$j" => $row[ "products[$j][B$j]" ],
                "C$j" => $row[ "products[$j][C$j]" ],
                "D$j" => $row[ "products[$j][D$j]" ],
                "E$j" => $quote[ 'discount' ],
                "F$j" => $row[ "products[$j][D$j]" ],
            ];

        }

        $quote['products'][ ++$j ] = [
            "A$j" => 0,
            "B$j" => __('Amount advanced (material)'),
            "C$j" => 1,
            "D$j" => $quote[ 'amount_advanced' ],
            "E$j" => 0,
            "F$j" => $quote[ 'amount_advanced' ],
        ];
        unset( $quote['expire_date'], $quote['exit_date'], $quote['pdf_path'], $quote['created'], $quote['data'] );

        return $quote;
    }

    public function afterSave(Event $event, Order $order, \ArrayObject $options)
    {
        if(!$order->isNew()){
            return;
        }
        try {
            if(empty($order->quote_id)){
                return;
            }
            $quote = $this->Quotes->get( $order->quote_id );
            $quote->order_id = $order->id;
            $quote->dirty('order_id', true);
            $this->Quotes->save($quote);
        } catch (Exception $e) {

        }
    }
}
