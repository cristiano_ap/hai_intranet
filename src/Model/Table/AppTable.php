<?php
namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * Clients Model
 *
 * @property \Cake\ORM\Association\HasMany $Orders
 */
class AppTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
	}
}