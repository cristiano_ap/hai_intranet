<?php
namespace App\Model\Table;

use App\Model\Entity\Client;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Clients Model
 *
 * @property \Cake\ORM\Association\HasMany $Orders
 */
class ClientsTable extends AppTable
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('clients');
        $this->displayField('identifier');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Orders', [
            'foreignKey' => 'client_id'
        ]);
        $this->hasMany('Quotes', [
            'foreignKey' => 'client_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('individual_registration', 'create')
            ->notEmpty('individual_registration')
            ->add('individual_registration', 'unique', [
                'rule' => 'validateUnique', 
                'provider' => 'table', 
                'message' => __('This individual registration is already registered')
            ])
            ->add('individual_registration', 'valid_format', [
                'rule' => [
                    'custom', 
                    '/([\d]{3}.[\d]{3}.[\d]{3}-[\d]{2}|[\d]{2,3}.[\d]{3}.[\d]{3}\/[\d]{4}-[\d]{2})/'
                ], 
                'message' => 'This format is invalid',
                'last' => true
            ])
            ->add('individual_registration', 'valid_hash', [
                'rule' => 'isValidFormatIndividualRegistration', 
                'message' => __('This individual registration is wrong'),
                'provider' => 'table'
            ]);

        $validator
            ->allowEmpty('state_registration');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->requirePresence('district', 'create')
            ->notEmpty('district');

        $validator
            ->requirePresence('zipcode', 'create')
            ->notEmpty('zipcode')
            ->add('zipcode', 'valid_format', [
                'rule' => [
                    'custom',
                    '/[\d]{5}[-]{0,1}[\d]{3}/'
                ],
                'message' => __('This zipcode is invalid')
            ]);

        $rule_phone = [
            'rule' => [
                'custom',
                '/^1\d\d(\d\d)?$|^0800 ?\d{3} ?\d{4}$|^(\(0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d\) ?|0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d[ .-]?)?(9|9[ .-])?[2-9]\d{3}[ .-]?\d{4}$/',
            ],
            'message' => __('The phone format is invalid')
        ];

        $validator
            ->allowEmpty('phone1')
            ->add('phone1', 'valid_format', $rule_phone);

        $validator
            ->allowEmpty('phone2')
            ->add('phone2', 'valid_format', $rule_phone);

        return $validator;
    }

    public function isValidFormatIndividualRegistration($value, array $context)
    {
        $cpf = $cnpj = ereg_replace('[^0-9]', '', $value);

        if (strlen($cpf) == Client::FORMAT_CPF_SIZE) {

            $black_list = [
                '00000000000',
                '11111111111',
                '22222222222',
                '33333333333',
                '44444444444',
                '55555555555',
                '66666666666',
                '77777777777',
                '88888888888',
                '99999999999'
            ];

            $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
            
            if (in_array($cpf, $black_list))
            {
                return false;
            }
            else
            {
                for ($t = 9; $t < 11; $t++) {
                    for ($d = 0, $c = 0; $c < $t; $c++) {
                        $d += $cpf{$c} * (($t + 1) - $c);
                    }
         
                    $d = ((10 * $d) % 11) % 10;
         
                    if ($cpf{$c} != $d) {
                        return false;
                    }
                }
         
                return true;
            }

        } else if(strlen($cnpj) > Client::FORMAT_CPF_SIZE) {

            $soma = 0;

            $soma += ($cnpj[0] * 5);
            $soma += ($cnpj[1] * 4);
            $soma += ($cnpj[2] * 3);
            $soma += ($cnpj[3] * 2);
            $soma += ($cnpj[4] * 9); 
            $soma += ($cnpj[5] * 8);
            $soma += ($cnpj[6] * 7);
            $soma += ($cnpj[7] * 6);
            $soma += ($cnpj[8] * 5);
            $soma += ($cnpj[9] * 4);
            $soma += ($cnpj[10] * 3);
            $soma += ($cnpj[11] * 2); 

            $d1 = $soma % 11; 
            $d1 = $d1 < 2 ? 0 : 11 - $d1; 

            $soma = 0;
            $soma += ($cnpj[0] * 6); 
            $soma += ($cnpj[1] * 5);
            $soma += ($cnpj[2] * 4);
            $soma += ($cnpj[3] * 3);
            $soma += ($cnpj[4] * 2);
            $soma += ($cnpj[5] * 9);
            $soma += ($cnpj[6] * 8);
            $soma += ($cnpj[7] * 7);
            $soma += ($cnpj[8] * 6);
            $soma += ($cnpj[9] * 5);
            $soma += ($cnpj[10] * 4);
            $soma += ($cnpj[11] * 3);
            $soma += ($cnpj[12] * 2); 


            $d2 = $soma % 11; 
            $d2 = $d2 < 2 ? 0 : 11 - $d2; 

            if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique([
            'individual_registration',
            'state_registration'
        ]));
        return $rules;
    }
}
