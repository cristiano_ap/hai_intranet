<?php
namespace App\Model\Table;

use App\Model\Entity\Quote;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Chronos\Chronos;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Cake\Database\Schema\Table as Schema;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * Quotes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clients
 * @property \Cake\ORM\Association\HasOne $Order
 */
class QuotesTable extends AppTable
{
    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('quotes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                ],
            ]
        ]);

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Orders', [
            // 'conditions' => ['Quote.accepted <> NULL'],
            'dependent' => true
        ]);
    }

    /**
     * Initialize schema method
     *
     * @param array $schema The schema for the Table.
     * @return schema
     */
    protected function _initializeSchema(Schema $schema)
    {
        $schema->columnType('data', 'json');
        return $schema;
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
        $validator
            ->add('client_id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('client_id');

        $validator
            ->add('final_price', 'valid', ['rule' => 'numeric'])
            ->requirePresence('final_price', 'create')
            ->notEmpty('final_price');

        $validator
            ->add('expire_date', 'valid', ['rule' => 'date'])
            ->requirePresence('expire_date', 'create')
            ->notEmpty('expire_date')
            ->add('expire_date', 'validDate', [
                'rule' => function ($data, $provider) {
                    $data = Chronos::createFromDate($data['year'], $data['month'], $data['day']);

                    // if (Chronos::now()->lte($data)) {
                    if (!$data->isPast()) {
                       return true;
                    }

                    return 'The expire date cannot be in the past';
                }
            ]);

        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->allowEmpty('warranty');

        $validator
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }


    public function generatePdf($quote)
    {
        try {
            $quote->client = $this->Clients->get($quote->client_id);

            $options = Configure::read('CakePdf') ? : [
                'engine' => 'CakePdf.Mpdf',
                // 'engine' => 'CakePdf.Tcpdf',
                'margin' => [
                    'bottom' => 15,
                    'left' => 15,
                    'right' => 30,
                    'top' => 30
                ],
                'orientation' => 'portrait',
            ];

            $CakePdf = new \CakePdf\Pdf\CakePdf($options);
            $CakePdf->template('quote.view', 'default');

            $name = Text::uuid() . ".pdf";
            $path = $url = Configure::read('App.paths.pdfs');
            $path = WWW_ROOT . DS . $path;

            $folder = new Folder($path);
            if($folder->create($path)){
                $folder->chmod($path, 0755, true);
            } else {
                throw new NotFoundException(__('Error on folder creation'));
            }

            $quote->pdf_path = str_ireplace('\\', '/', "/$url/$name");
            $quote->dirty('pdf_path', true);

            $CakePdf->viewVars([
                'quote' => $quote
            ]);
            $writed = $CakePdf->write($path . DS . $name);

            if(!$writed) {
                return false;
            }

            $this->save($quote);

            $event = new Event('Model.Quote.afterGeneratePDF', $this, [
                'quote' => $quote
            ]);
            $this->eventManager()->dispatch($event);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Update quote as accepted
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function setAsAccepted($quote)
    {
        $quote->accepted = new Time();
        $quote->dirty('accepted', true);
        return $this->save($quote);
    }
}
