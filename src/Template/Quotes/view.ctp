<?php

use Cake\Routing\Router;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Form->postLink(__('Delete Quote'), ['action' => 'delete', $quote->id], ['confirm' => __('Are you sure you want to delete # {0}?', $quote->id)]) ?> </li>
<?php if($quote->isAccepted() && $quote->becameOrder()): ?>
<li><?= $this->Html->link(__('View generated order'), ['controller' => 'Orders', 'action' => 'view', $quote->order_id]) ?></li>
<?php elseif($quote->isAccepted()): ?>
<li><?= $this->Form->postLink(__('Generate order from this quote'), ['action' => 'generate_order', $quote->id], ['confirm' => __('Are you sure?')]) ?></li>
<?php else: ?>
<li><?= $this->Form->postLink(__('Set as accepted'), ['action' => 'set_as_accepted', $quote->id], ['confirm' => __('Are you sure?')]) ?></li>
<?php endif; ?>
<?php if(is_null($quote->pdf_path)): ?>
<li><?= $this->Form->postLink(__('Generate PDF'), ['action' => 'generate_pdf', $quote->id]) ?> </li>
<?php endif; ?>
<li><?= $this->Html->link(__('New Quote'), ['controller' => 'Quotes', 'action' => 'add']) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __("Quote to {0}", $quote->client->name) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Client') ?></td>
            <td><?= $quote->has('client') ? $this->Html->link($quote->client->identifier, ['controller' => 'Clients', 'action' => 'view', $quote->client->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= h($quote->id_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Accepted') ?></td>
            <?php
               if($quote->isAccepted()):
            ?>
            <td><i class="glyphicon glyphicon-ok"></i> <?= $quote->accepted_formatted ?></td>
            <?php else: ?>
            <td><i class="glyphicon glyphicon-remove"></i></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td><?= __('Final Price') ?></td>
            <td><?= h($quote->final_price_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Expire Date') ?></td>
            <td><?= h($quote->expire_date_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Exit Date') ?></td>
            <td><?= h($quote->exit_date_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($quote->created) ?></td>
        </tr>
    <?php if(!empty($quote->pdf_path)): ?>
        <tr>
            <td colspan="1"><?= __('Attachment') ?></td>
            <?php
                $pdf_name = __("Quote {0} - {1}", $quote->id_formatted, $quote->client->name);
            ?>
            <td colspan="1"><?= $this->Html->link(__('Download'), $quote->pdf_path, ['download' => $pdf_name . '.pdf']) ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <embed width="100%" height="600" src="<?php echo Router::url($quote->pdf_path) ?>" type="application/pdf">            </embed>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td><?= __('Warranty') ?></td>
            <td><?= h(nl2br($quote->warranty)) ?></td>
        </tr>
        <tr>
            <td><?= __('Location') ?></td>
            <td><?= h(nl2br($quote->location)) ?></td>
        </tr>
        <tr>
            <td colspan="2"><?= __('Data of the products') ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="table">
                    <thead>
                        <tr>
                            <td style="width: 10%" ><label><?php echo __('Id') ?></label></td>
                            <td style="width: 40%"><label><?php echo __('Name') ?></label></td>
                            <td style="width: 8%" ><label><?php echo __('Quantity') ?></label></td>
                            <td style="width: 10%" ><label><?php echo __('Price Unit') ?></label></td>
                            <td style="width: 14%" ><label><?php echo __('Total Price') ?></label></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?= $this->Html->tableCells($quote->data) ?>
                    </tbody>
                </table>
            </td>
        </tr>
    <?php endif; ?>
    </table>
</div>

