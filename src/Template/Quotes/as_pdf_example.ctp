<?php

// use Cake\Routing\Router;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Form->postLink(__('Delete Order'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?> </li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<?php
use Cake\Routing\Router;
use Cake\Core\Configure;

$path = Configure::read('App.wwwRoot');

?>

<table width="100%" style="border:solid 1px">
    <tbody>
        <tr>
            <td colspan="6" style="text-align:center;">
                Informações do Emitente
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;">
                <img src="<?php echo Router::url('/img/hai.logo.dark.png') ?>" alt="Logo Hai Tecnologia" class="logo">
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align:center;">
                Hai- Soluções em Tecnologia <br>
                Rua Quatorze de Junho,74-Buritis,30575-350 <br>
                Belo Horizonte-MG Fone:(31)9244-9495 <br>
                <a href="http://www.haitecnologia.com">www.haitecnologia.com</a> <br>
            </td>
        </tr>
    </tbody>
</table>
<table width="100%" border="1">
    <caption>DESTINAT&Aacute;RIO/REMETENTE:</caption>
    <tbody>
        <tr>
            <td colspan="3" class="no border bottom">NOME/RAZ&Atilde;O SOCIAL:&nbsp;</td>
            <td class="no border bottom">CPF/CNPJ:</td>
            <td colspan="2" class="no border bottom">DATA DA EMISS&Atilde;O:</td>
        </tr>
        <tr>
            <td colspan="3" class="no border top value"><?= h($order->client->name) ?></td>
            <td class="no border top value"><?= h($order->individual_registration_formatted) ?></td>
            <td colspan="2" class="no border top value"><?= h($order->created_formatted) ?></td>
        </tr>
        <tr>
            <td colspan="2" class="no border bottom">ENDERE&Ccedil;O:</td>
            <td class="no border bottom">BAIRRO:</td>
            <td class="no border bottom">CEP:</td>
            <td colspan="2" class="no border bottom">DATA DE SA&Iacute;DA:</td>
        </tr>
        <tr>
            <td colspan="2" class="no border top value"><?= h($order->client->address) ?></td>
            <td class="no border top value"><?= h($order->client->district) ?></td>
            <td class="no border top value"><?= h($order->client->zipcode_formatted) ?></td>
            <td colspan="2" class="no border top value"><?= $order->exit_date_formatted ?></td>
        </tr>
        <tr>
            <td class="no border bottom">MUNIC&Iacute;PIO:</td>
            <td class="no border bottom">UF:</td>
            <td class="no border bottom">FONE/FAX:</td>
            <td colspan="3" class="no border bottom">INSCRI&Ccedil;&Atilde;O ESTADUAL</td>
        </tr>
        <tr>
            <td class="no border top value"><?= h($order->client->city) ?></td>
            <td class="no border top value"><?= h($order->client->state) ?></td>
            <td class="no border top value"><?= h($order->client->phone1) ?></td>
            <td colspan="3" class="no border top value"><?= h($order->client->state_registration) ?></td>
        </tr>
    </tbody>
</table>
<table width="100%" border="1">
    <caption>FATURA/DUPLICATA:</caption>
    <tbody>
        <tr>
            <td width="100">N&Uacute;MERO:</td>
            <td colspan="5" class="value"><?= h($order->id_formatted) ?></td>
        </tr>
        <tr>
            <td>VENCIMENTO:</td>
            <td colspan="5" class="value"><?= h($order->expire_date_formatted) ?></td>
        </tr>
        <tr>
            <td>VALOR:</td>
            <td colspan="5" class="value"><?= h($order->final_price_formatted) ?></td>
        </tr>
    </tbody>
</table>
<table width="100%" border="1">
    <caption>DADOS DOS PRODUTOS:</caption>
    <tbody>
        <tr style="text-align:center;">
            <td>C&Oacute;DIGO:</td>
            <td>DESCRI&Ccedil;&Atilde;O:</td>
            <td>QUANTIDADE:</td>
            <td>VALOR UNIT:</td>
            <td>DESCONTO:</td>
            <td>VALOR TOTAL:</td>
        </tr>
        <?php
            echo $this->Html->tableCells($order->data);
        ?>
    </tbody>
</table>
<table width="100%" border="1">
    <caption>DADOS ADICIONAIS:</caption>
    <tbody>
        <tr>
            <td>LOCAL DO SERVI&Ccedil;O:</td>
            <td>GARANTIA:</td>
        </tr>
        <tr>
            <td width="50%">
                <p class="padding-20">
                <?= h($order->location) ?>
                </p>
            </td>
            <td>
                <p class="padding-20">
                <?= h($order->warranty) ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<table width="100%" border="1" style="text-align: center;">
    <caption style="text-align: center;">*N&Atilde;O &Eacute; NOTA FISCAL, APENAS RECIBO</caption>
    <tbody>
        <tr>
            <td colspan="6" style="text-align: center;">
                <p>ASSINATURA INSTALADOR:</p>
                <p>&nbsp;</p>
                <hr style="width:90%;margin:auto;border:solid 1px">
            </td>
        </tr>
    </tbody>
</table>