<?php

use \Cake\I18n\Time;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Quote'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('client_id'); ?></th>
            <th><?= $this->Paginator->sort('final_price'); ?></th>
            <th><?= $this->Paginator->sort('expire_date'); ?></th>
            <th><?= $this->Paginator->sort('paid'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($quotes as $quote): ?>
        <tr>
            <td><?= h($quote->id_formatted) ?></td>
            <td>
                <?= $quote->has('client') ? $this->Html->link($quote->client->name, ['controller' => 'Clients', 'action' => 'view', $quote->client->id]) : '' ?>
            </td>
            <td><?= h($quote->final_price_formatted) ?></td>
            <?php
                $now = (new Time())->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
                $expire = (new Time($quote->expire_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
                if($now > $expire && !$quote->isAccepted()){
                    $style_field_expire_formatted = 'bg-danger';
                } else if($now == $expire && !$quote->isAccepted()){
                    $style_field_expire_formatted = 'bg-warning';
                } else {
                    $style_field_expire_formatted = 'bg-success';
                }
            ?>
            <td class="<?= $style_field_expire_formatted ?>"><?= h($quote->expire_date_formatted) ?></td>
            <?php
               $tick = $quote->isAccepted() ? 'ok' : 'remove';
            ?>
            <td class="<?= $style_field_expire_formatted ?>"><i class="glyphicon glyphicon-<?= $tick ?>"></i></td>
            <td class="actions">
                <?php if($quote->isAccepted() && $quote->becameOrder()): ?>
                <?= $this->Html->link('', ['controller' => 'Orders', 'action' => 'view', $quote->order_id], ['title' => __('View generated order'), 'class' => 'btn btn-default glyphicon glyphicon-barcode']) ?>
                <?php elseif($quote->isAccepted()): ?>
                <?= $this->Form->postLink('', ['action' => 'generate_order', $quote->id], ['confirm' => __('Are you sure?'), 'title' => __('Generate order from this quote'), 'class' => 'btn btn-default glyphicon glyphicon-list-alt']) ?>
                <?php else: ?>
                <?= $this->Form->postLink('', ['action' => 'set_as_accepted', $quote->id], ['confirm' => __('Are you sure?'), 'title' => __('Set as accepted'), 'class' => 'btn btn-default glyphicon glyphicon-check']) ?>
                <?php endif; ?>
                <?= $this->Html->link('', ['action' => 'view', $quote->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $quote->id], ['confirm' => __('Are you sure you want to delete # {0}?', $quote->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>