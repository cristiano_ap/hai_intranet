<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<?= $this->Form->create($quote, ['id' => 'form-quote-add', 'autocomplete' => 'off']); ?>
<?= $this->cell('UserSearch') ?>
<?= $this->cell('ProductSelector', ['.product-name']) ?>
<fieldset>
    <legend><?= __('Quote') ?></legend>
    <?php
    echo $this->Form->input('expire_date', [
        'day' => [
            'class' => 'no-chosen'
        ],
        'month' => [
            'class' => 'no-chosen'
        ],
        'year' => [
            'class' => 'no-chosen'
        ],
    ]);
    echo $this->Form->input('exit_date', [
        'day' => [
            'class' => 'no-chosen'
        ],
        'month' => [
            'class' => 'no-chosen'
        ],
        'year' => [
            'class' => 'no-chosen'
        ],
    ]);

    echo $this->Form->hidden('data', [
        'id' => 'products-data'
    ]);
?>
</fieldset>
<fieldset>
    <legend><?= __('Data of the products') ?></legend>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" id="dynamic">
                <table class="table">
                    <thead>
                        <tr>
                            <!-- <td style="width: 10%" class="text-center"><label><?php echo __('Id') ?></label></td> -->
                            <td colspan="2" style="width: 50%"><label><?php echo __('Name') ?></label></td>
                            <td style="width: 8%" class="text-center"><label><?php echo __('Quantity') ?></label></td>
                            <td style="width: 10%" class="text-center"><label><?php echo __('Price Unit') ?></label></td>
                            <td style="width: 14%" class="text-center"><label><?php echo __('Total Price') ?></label></td>
                            <td style="width: 5%" class="text-center"><label><?php echo __('Delete') ?></label></td>
                        </tr>
                    </thead>
                    <tbody id="itemlist">
                        <?php
                            if(isset($this->request->data['products'])) :
                                foreach ($this->request->data['products'] as $row => $cell):
                        ?>
                        <tr>
                            <td colspan="2"><input class="form-control input-sm text-right product-id" data-cell="A<?php echo $row ?>" data-format="0" name="products[<?php echo $row ?>][A<?php echo $row ?>]" type="hidden">
                            <input class="form-control input-sm product-name" data-cell="B<?php echo $row ?>" required name="products[<?php echo $row ?>][B<?php echo $row ?>]" value="<?php echo $cell["B$row"] ?>"></td><!-- NAME -->
                            <td><input class="form-control input-sm text-right product-qty" data-cell="C<?php echo $row ?>" data-format="0[.]00" required name="products[<?php echo $row ?>][C<?php echo $row ?>]" value="<?php echo $cell["C$row"] ?>"></td><!-- QTDE -->
                            <td><input class="form-control input-sm text-right product-price" data-cell="D<?php echo $row ?>" data-format="$ 0,0.00" required name="products[<?php echo $row ?>][D<?php echo $row ?>]" value="<?php echo $cell["D$row"] ?>"></td><!-- UNIT -->
                            <td><input class="form-control input-sm text-right" data-cell="F<?php echo $row ?>" data-format="$ 0,0.00" data-formula="D<?php echo $row ?>*C<?php echo $row ?>" required  name="products[<?php echo $row ?>][F<?php echo $row ?>]" value="<?php echo $cell["F$row"] ?>"></td><!-- TOTAL -->
                            <td class="text-center"><button class="btn-remove btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></button></td><!-- DELETE -->
                        </tr>
                        <?php
                                endforeach;
                            endif;
                        ?>
                    </tbody>
                    <script>$counter = <?php echo isset($row) ? $row : 0?></script>
                    <tfoot>
                        <tr style="margin-top:30px">
                            <td>
                                <button id="add_item" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i></button>
                            </td>
                            <td colspan="3" class="text-right">
                                <label for="total"><?= __('Total Price') ?> :</label>
                            </td>
                            <td class="text-right">
                                <!-- <input class="form-control input-sm text-right" data-cell="G2" data-format="0,0[.]00 %" required name="products[2][G2]" value="<?= @$quote->discount ?>" name="discount" id="discount"> -->
                                <label data-cell="G1" data-format="$ 0,0.00" data-formula="SUM(F1:F999)"></label>
                                <input class="form-control input-sm text-right" data-cell="G1" data-format="$ 0,0.00" data-formula="SUM(F1:F999)" id="total-price">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                <label for="discount_formatted"><?= __('Discount') ?> :</label>
                            </td>
                            <td class="text-right">
                                <!-- <input class="form-control input-sm text-right" data-cell="G2" data-format="0,0[.]00 %" required name="products[2][G2]" value="<?= @$quote->discount ?>" name="discount" id="discount"> -->
                                <?php
                                echo $this->Form->input('discount_formatted',[
                                    'label' => false,
                                    'div' => false,
                                    'data-format' => '0,0[.]00 %',
                                    'data-cell' => 'G2',
                                    'id' => 'discount_formatted',
                                    'templates' => [
                                        'inputContainer' => '{{content}}',
                                        'input' => '<input type="{{type}}" name="{{name}}" class ="form-control input-sm text-right" {{attrs}}/>'
                                    ]
                                ]);
                                echo $this->Form->hidden('discount',[
                                    'label' => false,
                                    'div' => false,
                                    'data-format' => '0,0[.]00',
                                    'data-cell' => 'H2',
                                    'data-formula' => 'G2*100',
                                    'id' => 'discount',
                                ]);
                                ?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                <label for="subtotal"><?= __('Subtotal') ?> :</label>
                            </td>
                            <td class="text-right">
                                <input class="form-control input-sm text-right" data-cell="G3" data-format="$ 0,0.00" data-formula="G1-(G1*G2)" id="subtotal">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                <label for="advanced"><?= __('Amount advanced (material)') ?> :</label>
                            </td>
                            <td class="text-right">
                                <!-- <input class="form-control input-sm text-right" data-cell="G4" data-format="$ 0,0.00" required name="amount_advanced" value="<?= @$quote->amount_advanced ?>" id="advanced"> -->
                                <?php
                                echo $this->Form->input('amount_advanced_formatted', [
                                    'label' => false,
                                    'div' => false,
                                    'type' => 'text',
                                    'required' => true,
                                    'data-format' => '$ 0,0.00',
                                    'data-cell' => 'G4',
                                    'id' => 'advanced',
                                    'templates' => [
                                        'inputContainer' => '{{content}}',
                                        'input' => '<input type="{{type}}" name="{{name}}" class ="form-control input-sm text-right" {{attrs}}/>'
                                    ]
                                ]);
                                echo $this->Form->hidden('amount_advanced', [
                                    'label' => false,
                                    'div' => false,
                                    'required' => true,
                                    'data-format' => '0,0[.]00',
                                    'data-formula' => 'G4',
                                    'data-cell' => 'H4',
                                ]);
                                ?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                <label for=""><?= __('Final Price') ?></label> :
                            </td>
                            <td class="text-right">
                                <!-- <input class="form-control input-sm text-right" data-cell="G5" data-format="$ 0,0.00" data-formula="G3+G4" required name="products[5][G5]" value="<?= @$cell["G5"] ?>"> -->
                                <label data-cell="G5" data-format="$ 0,0.00" data-formula="G3+G4"></label>
                                <?php
                                echo $this->Form->hidden('final_price', [
                                    'label' => false,
                                    'div' => false,
                                    'data-format' => '0.00',
                                    'data-cell' => 'Z1',
                                    'data-formula' => 'G5',
                                    'id' => 'final_price'
                                ]);
                                ?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend><?= __('Aditional Information') ?></legend>
    <?php
    echo $this->Form->input('location', [
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>'
        ]
    ]);
    echo $this->Form->input('warranty', [
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>'
        ]
    ]);
    ?>
</fieldset>
<?= $this->Form->button(__("Place Order")); ?>
<?= $this->Form->end() ?>
