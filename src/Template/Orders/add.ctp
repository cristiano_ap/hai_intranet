<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<?= $this->Form->create($order, ['id' => 'form-order-add', 'autocomplete' => 'off', 'url' => ['action' => 'add']]); ?>
<?= $this->cell('UserSearch') ?>
<?= $this->cell('ProductSelector', ['.product-name']) ?>
<fieldset>
    <legend><?= __('Order') ?></legend>
    <?php
    echo $this->Form->input('expire_date', [
        'day' => [
            'class' => 'no-chosen'
        ],
        'month' => [
            'class' => 'no-chosen'
        ],
        'year' => [
            'class' => 'no-chosen'
        ],
    ]);
    echo $this->Form->input('exit_date', [
        'day' => [
            'class' => 'no-chosen'
        ],
        'month' => [
            'class' => 'no-chosen'
        ],
        'year' => [
            'class' => 'no-chosen'
        ],
    ]);
    echo $this->Form->hidden('quote_id', [
        'value' => (int) @$this->request->query['from_quote']
    ]);
    ?>
</fieldset>
<fieldset>
    <legend><?= __('Data of the products') ?></legend>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" id="dynamic">
                <table class="table">
                    <thead>
                        <tr>
                            <!-- <td style="width: 10%" class="text-center"><label><?php echo __('Id') ?></label></td> -->
                            <td colspan="2" style="width: 50%"><label><?php echo __('Name') ?></label></td>
                            <td style="width: 8%" class="text-center"><label><?php echo __('Quantity') ?></label></td>
                            <td style="width: 10%" class="text-center"><label><?php echo __('Price Unit') ?></label></td>
                            <td style="width: 8%" class="text-center"><label><?php echo __('Discount') ?></label></td>
                            <td style="width: 14%" class="text-center"><label><?php echo __('Total Price') ?></label></td>
                            <td style="width: 5%" class="text-center"><label><?php echo __('Delete') ?></label></td>
                        </tr>
                    </thead>
                    <tbody id="itemlist">
                        <?php
                            if(isset($this->request->data['products'])) :
                                foreach ($this->request->data['products'] as $row => $cell):
                        ?>
                        <tr>
                            <td><input class="form-control input-sm text-right" data-cell="A<?php echo $row ?>" data-format="0" required name="products[<?php echo $row ?>][A<?php echo $row ?>]" value="<?php echo $cell["A$row"] ?>"></td>
                            <td><input class="form-control input-sm" data-cell="B<?php echo $row ?>" required name="products[<?php echo $row ?>][B<?php echo $row ?>]" value="<?php echo $cell["B$row"] ?>"></td>
                            <td><input class="form-control input-sm text-right" data-cell="C<?php echo $row ?>" data-format="0[.]00" required name="products[<?php echo $row ?>][C<?php echo $row ?>]" value="<?php echo $cell["C$row"] ?>"></td>
                            <td><input class="form-control input-sm text-right" data-cell="D<?php echo $row ?>" data-format="$ 0,0.00" required name="products[<?php echo $row ?>][D<?php echo $row ?>]" value="<?php echo $cell["D$row"] ?>"></td>
                            <td><input class="form-control input-sm text-right" data-cell="E<?php echo $row ?>" data-format="0,0[.]00 %" name="products[<?php echo $row ?>][E<?php echo $row ?>]" value="<?php echo $cell["E$row"] ?>"></td>
                            <td><input class="form-control input-sm text-right" data-cell="F<?php echo $row ?>" data-format="$ 0,0.00" data-formula="D<?php echo $row ?>*(C<?php echo $row ?>-(C<?php echo $row ?>*E<?php echo $row ?>))" required  name="products[<?php echo $row ?>][F<?php echo $row ?>]" value="<?php echo $cell["F$row"] ?>"></td>
                            <td class="text-center"><button class="btn-remove btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></button></td>
                        </tr>
                        <?php
                                endforeach;
                            endif;
                        ?>
                    </tbody>
                    <script>$counter = <?php echo isset($row) ? $row : 0?></script>
                    <tfoot>
                        <tr style="margin-top:30px">
                            <td>
                                <button id="add_item" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i></button>
                            </td>
                            <td colspan="4" class="text-right">
                                <label for="total"><?= __('Total Price') ?> - <?= __('Discount') ?> :</label>
                            </td>
                            <td class="text-right">
                                <label data-cell="G1" data-format="$ 0,0.00" data-formula="SUM(F1:F999)"></label>
                                <?php

                                echo $this->Form->hidden('final_price', [
                                    'label' => false,
                                    'div' => false,
                                    'data-format' => '0.00',
                                    'data-cell' => 'Z1',
                                    'data-formula' => 'G1',
                                    'id' => 'final_price'
                                ]);
                                echo $this->Form->hidden('data', [
                                    'id' => 'products-data'
                                ]);

                                ?>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend><?= __('Aditional Information') ?></legend>
    <?php
    echo $this->Form->input('location', [
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>'
        ]
    ]);
    echo $this->Form->input('warranty', [
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>'
        ]
    ]);
    ?>
</fieldset>
<?= $this->Form->button(__("Place Order")); ?>
<?= $this->Form->end() ?>
