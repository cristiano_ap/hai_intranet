<?php

use Cake\Routing\Router;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Form->postLink(__('Delete Order'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?> </li>
<?php if(!$order->isPaid() && !is_null($order->pdf_path)): ?>
<li><?= $this->Form->postLink(__('Set as Paid'), ['action' => 'set_as_paid', $order->id], ['confirm' => __('Are you sure?')]) ?> </li>
<?php endif; ?>
<?php if(is_null($order->pdf_path)): ?>
<li><?= $this->Form->postLink(__('Generate PDF'), ['action' => 'generate_pdf', $order->id]) ?> </li>
<?php endif; ?>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __("Order to {0}", $order->client->name) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Client') ?></td>
            <td><?= $order->has('client') ? $this->Html->link($order->client->identifier, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= h($order->id_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Paid') ?></td>
            <?php
               if($order->isPaid()):
            ?>
            <td><i class="glyphicon glyphicon-ok"></i> <?= $order->paid_formatted ?></td>
            <?php else: ?>
            <td><i class="glyphicon glyphicon-remove"></i></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td><?= __('Final Price') ?></td>
            <td><?= h($order->final_price_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Expire Date') ?></td>
            <td><?= h($order->expire_date_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Exit Date') ?></td>
            <td><?= h($order->exit_date_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($order->created) ?></td>
        </tr>
    <?php if(!empty($order->pdf_path)): ?>
        <tr>
            <td colspan="1"><?= __('Attachment') ?></td>
            <?php
                $pdf_name = __("Order {0} - {1}", $order->id_formatted, $order->client->name);
            ?>
            <td colspan="1"><?= $this->Html->link(__('Download'), $order->pdf_path, ['download' => $pdf_name . '.pdf']) ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <embed width="100%" height="600" src="<?php echo Router::url($order->pdf_path) ?>" type="application/pdf">            </embed>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td><?= __('Warranty') ?></td>
            <td><?= h(nl2br($order->warranty)) ?></td>
        </tr>
        <tr>
            <td><?= __('Location') ?></td>
            <td><?= h(nl2br($order->location)) ?></td>
        </tr>
        <tr>
            <td colspan="2"><?= __('Data of the products') ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="table">
                    <thead>
                        <tr>
                            <td style="width: 10%" ><label><?php echo __('Id') ?></label></td>
                            <td style="width: 40%"><label><?php echo __('Name') ?></label></td>
                            <td style="width: 10%" ><label><?php echo __('Price Unit') ?></label></td>
                            <td style="width: 8%" ><label><?php echo __('Quantity') ?></label></td>
                            <td style="width: 8%" ><label><?php echo __('Discount') ?></label></td>
                            <td style="width: 14%" ><label><?php echo __('Total Price') ?></label></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?= $this->Html->tableCells($order->data) ?>
                    </tbody>
                </table>
            </td>
        </tr>
    <?php endif; ?>
    </table>
</div>

