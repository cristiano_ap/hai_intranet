<?php

use \Cake\I18n\Time;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Order'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('client_id'); ?></th>
            <th><?= $this->Paginator->sort('final_price'); ?></th>
            <th><?= $this->Paginator->sort('expire_date'); ?></th>
            <th><?= $this->Paginator->sort('paid'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($orders as $order): ?>
        <tr>
            <td><?= h($order->id_formatted) ?></td>
            <td>
                <?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?>
            </td>
            <td><?= h($order->final_price_formatted) ?></td>
            <?php
                $now = (new Time())->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
                $expire = (new Time($order->expire_date))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE]);
                if($now > $expire && !$order->isPaid()){
                    $style_field_expire_formatted = 'bg-danger';
                } else if($now == $expire && !$order->isPaid()){
                    $style_field_expire_formatted = 'bg-warning';
                } else {
                    $style_field_expire_formatted = 'bg-success';
                }
            ?>
            <td class="<?= $style_field_expire_formatted ?>"><?= h($order->expire_date_formatted) ?></td>
            <?php
               $tick = $order->isPaid() ? 'ok' : 'remove';
            ?>
            <td class="<?= $style_field_expire_formatted ?>"><i class="glyphicon glyphicon-<?= $tick ?>"></i></td>
            <td class="actions">
                <?php if($order->isPaid()): ?>
                <?= $this->Html->link('', '#', ['title' => __('Set as paid at {0}', $order->paid_formatted), 'class' => 'btn btn-default glyphicon glyphicon-usd', 'disabled' => true]) ?>
                <?php else: ?>
                <?= $this->Form->postLink('', ['action' => 'set_as_paid', $order->id], ['confirm' => __('Are you sure?'), 'title' => __('Set as paid'), 'class' => 'btn btn-default glyphicon glyphicon-usd']) ?>
                <?php endif; ?>
                <?= $this->Html->link('', ['action' => 'view', $order->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>