<?php
use Cake\Routing\Router;
use Cake\Core\Configure;

$path = Configure::read('App.wwwRoot');

?>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td style="text-align:center;">
                Informações do Emitente <br>
				<img src="<?php echo $path . DS . 'img' . DS . 'hai.logo.dark.png' ?>" alt="Logo Hai Tecnologia" class="logo"> <br>
                Hai - Soluções em Tecnologia <br>
                Rua Quatorze de Junho, 74 - Buritis, 30575-350 <br>
                Belo Horizonte - MG Fone:(31) 9244-9495 <br>
                <a href="http://www.haitecnologia.com">www.haitecnologia.com</a>
            </td>
        </tr>
    </tbody>
</table>
</div>
<h3>DESTINAT&Aacute;RIO/REMETENTE:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td colspan="3" width="50%">
            NOME/RAZ&Atilde;O SOCIAL:&nbsp;<br>
            <b><?= h($quote->client->name) ?></b>
            </td>
            <td width="25%">
            CPF/CNPJ:<br>
            <b><?= h($quote->client->individual_registration_formatted) ?></b>
            </td>
            <td colspan="2" width="25%">
            DATA DA EMISS&Atilde;O:<br>
            <b><?= h($quote->created_formatted) ?></b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            ENDERE&Ccedil;O:<br>
            <b><?= h($quote->client->address) ?></b>
            </td>
            <td>
            BAIRRO:<br>
        	<b><?= h($quote->client->district) ?></b>
            </td>
            <td>
            CEP:<br>
            <b><?= h($quote->client->zipcode_formatted) ?></b>
            </td>
            <td colspan="2">
            DATA DE SA&Iacute;DA:<br>
            <b><?= $quote->exit_date_formatted ?></b>
            </td>
        </tr>
        <tr>
            <td>
            MUNIC&Iacute;PIO:<br>
            <b><?= h($quote->client->city) ?></b>
            </td>
            <td>
            UF:<br>
            <b><?= h($quote->client->state) ?></b>
            </td>
            <td>
            FONE/FAX:<br>
            <b><?= h($quote->client->phone1) ?></b>
            </td>
            <td colspan="3">
            INSCRI&Ccedil;&Atilde;O ESTADUAL: <br>
            <b><?= h($quote->client->state_registration_formatted) ?></b>
            </td>
        </tr>
    </tbody>
</table>
</div>
<h3>ORÇAMENTO:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td width="20%">N&Uacute;MERO:</td>
            <td width="80%"><b><?= h($quote->id_formatted) ?></b></td>
        </tr>
        <tr>
            <td>VENCIMENTO:</td>
            <td><b><?= h($quote->expire_date_formatted) ?></b></td>
        </tr>
        <tr>
            <td>VALOR:</td>
            <td><b><?= h($quote->final_price_formatted) ?></b></td>
        </tr>
    </tbody>
</table>
</div>
<h3>DADOS DOS PRODUTOS:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
	<thead>
        <tr>
            <th width="10%">C&Oacute;DIGO:</th>
            <th width="52%">DESCRI&Ccedil;&Atilde;O:</th>
            <th width="10%">QTDE:</th>
            <th width="13%">VALOR UNIT:</th>
            <th width="15%">VALOR TOTAL:</th>
        </tr>
	</thead>
    <tbody>
        <?php
            echo $this->Html->tableCells($quote->data['items']);
        ?>
    </tbody>
    <tfoot>
        <tr style="margin-top:30px">
            <td colspan="4" class="text-center">
                <?= __('Total Price') ?> :
            </td>
            <td class="text-right">
                <?= $quote->data['total_price'] ?>
            </td>
        </tr>
        <?php if($quote->discount): ?>
        <tr>
            <td colspan="4" class="text-center">
                <?= __('Discount') ?> :
            </td>
            <td class="text-right">
                <?= $quote->discount_formatted ?>
            </td>
        </tr>
        <?php endif ?>
        <?php if($quote->amount_advanced): ?>
        <tr>
            <td colspan="4" class="text-center">
                <?= __('Subtotal') ?> :
            </td>
            <td class="text-right">
                <?= $quote->data['subtotal'] ?>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-center">
                <?= __('Amount advanced (material)') ?> :
            </td>
            <td class="text-right">
                <?= $quote->amount_advanced_formatted ?>
            </td>
        </tr>
        <?php endif ?>
        <tr>
            <td colspan="4" class="text-center">
                <?= __('Final Price') ?> :
            </td>
            <td class="text-right">
                <?= $quote->final_price_formatted ?>
            </td>
        </tr>
    </tfoot>
</table>
</div>
<h3>DADOS ADICIONAIS:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td width="50%">
            LOCAL DO SERVI&Ccedil;O:<br>
            <?= h($quote->location) ?>
            </td>
            <td width="50%">
            GARANTIA:<br>
            <?= h($quote->warranty) ?>
            </td>
        </tr>
    </tbody>
</table>
</div>