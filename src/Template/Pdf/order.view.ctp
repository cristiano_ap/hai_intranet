<?php
use Cake\Routing\Router;
use Cake\Core\Configure;

$path = Configure::read('App.wwwRoot');

?>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td style="text-align:center;">
                Informações do Emitente <br>
				<img src="<?php echo $path . DS . 'img' . DS . 'hai.logo.dark.png' ?>" alt="Logo Hai Tecnologia" class="logo"> <br>
                Hai - Soluções em Tecnologia <br>
                Rua Quatorze de Junho, 74 - Buritis, 30575-350 <br>
                Belo Horizonte - MG Fone:(31) 9244-9495 <br>
                <a href="http://www.haitecnologia.com">www.haitecnologia.com</a>
            </td>
        </tr>
    </tbody>
</table>
</div>
<h3>DESTINAT&Aacute;RIO/REMETENTE:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td colspan="3" width="50%">
            NOME/RAZ&Atilde;O SOCIAL:&nbsp;<br>
            <b><?= h($order->client->name) ?></b>
            </td>
            <td width="25%">
            CPF/CNPJ:<br>
            <b><?= h($order->client->individual_registration_formatted) ?></b>
            </td>
            <td colspan="2" width="25%">
            DATA DA EMISS&Atilde;O:<br>
            <b><?= h($order->created_formatted) ?></b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            ENDERE&Ccedil;O:<br>
            <b><?= h($order->client->address) ?></b>
            </td>
            <td>
            BAIRRO:<br>
        	<b><?= h($order->client->district) ?></b>
            </td>
            <td>
            CEP:<br>
            <b><?= h($order->client->zipcode_formatted) ?></b>
            </td>
            <td colspan="2">
            DATA DE SA&Iacute;DA:<br>
            <b><?= $order->exit_date_formatted ?></b>
            </td>
        </tr>
        <tr>
            <td>
            MUNIC&Iacute;PIO:<br>
            <b><?= h($order->client->city) ?></b>
            </td>
            <td>
            UF:<br>
            <b><?= h($order->client->state) ?></b>
            </td>
            <td>
            FONE/FAX:<br>
            <b><?= h($order->client->phone1) ?></b>
            </td>
            <td colspan="3">
            INSCRI&Ccedil;&Atilde;O ESTADUAL: <br>
            <b><?= h($order->client->state_registration_formatted) ?></b>
            </td>
        </tr>
    </tbody>
</table>
</div>
<h3>FATURA/DUPLICATA:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td width="20%">N&Uacute;MERO:</td>
            <td width="80%"><b><?= h($order->id_formatted) ?></b></td>
        </tr>
        <tr>
            <td>VENCIMENTO:</td>
            <td><b><?= h($order->expire_date_formatted) ?></b></td>
        </tr>
        <tr>
            <td>VALOR:</td>
            <td><b><?= h($order->final_price_formatted) ?></b></td>
        </tr>
    </tbody>
</table>
</div>
<h3>DADOS DOS PRODUTOS:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
	<thead>
        <tr>
            <th width="10%">C&Oacute;DIGO:</th>
            <th width="40%">DESCRI&Ccedil;&Atilde;O:</th>
            <th width="10%">QTDE:</th>
            <th width="13%">VALOR UNIT:</th>
            <?php if($order->hasDiscount()): ?>
            <th width="12%">DESCONTO:</th>
            <?php endif; ?>
            <th width="15%">VALOR TOTAL:</th>
        </tr>
	</thead>
    <tbody>
        <?php
            echo $this->Html->tableCells($order->data_formatted);
        ?>
    </tbody>
</table>
</div>
<h3>DADOS ADICIONAIS:</h3>
<div>
<table width="100%" border="1" cellpadding="3">
    <tbody>
        <tr>
            <td width="50%">
            LOCAL DO SERVI&Ccedil;O:<br>
            <?= h($order->location) ?>
            </td>
            <td width="50%">
            GARANTIA:<br>
            <?= h($order->warranty) ?>
            </td>
        </tr>
    </tbody>
</table>
</div>
<div style="text-align: center;">*N&Atilde;O &Eacute; NOTA FISCAL, APENAS RECIBO</div>
<div>
<table width="100%" border="1" cellpadding="3" style="text-align: center;">
    <tbody>
        <tr>
            <td colspan="6" style="text-align: center;vertical-align:top;">
                ASSINATURA INSTALADOR: 
                <br>
                <br>
                ________________________________________________________________________
            </td>
        </tr>
    </tbody>
</table>
</div>
