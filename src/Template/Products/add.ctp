<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<?= $this->Form->create($product); ?>
<fieldset>
    <legend><?= __('Add {0}', [__('Product')]) ?></legend>
    <?php
    echo $this->Form->input('name', [
        'placeholder' => 'Cabo UTP',
        'type' => 'text',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-8 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-8">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('sku', [
        'type' => 'text',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('unit', [
        'type' => 'text',
        // 'placeholder' => '12345-678',
        // 'value' => $product->unit,
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('price', [
        'type' => 'number',
        // 'placeholder' => '99 12345-6789/99 1234-5678',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    ?>
</fieldset>
<?= $this->Form->submit(__("Add")); ?>
<?= $this->Form->end() ?>
