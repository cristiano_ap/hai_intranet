<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->name)]) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($product->name) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Name') ?></td>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= h($product->id_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('SKU') ?></td>
            <td><?= h($product->sku) ?></td>
        </tr>
        <tr>
            <td><?= __('Unit') ?></td>
            <td><?= h($product->unit) ?></td>
        </tr>
        <tr>
            <td><?= __('Price') ?></td>
            <td><?= h($product->price_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($product->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($product->modified) ?></td>
        </tr>
    </table>
</div>
