<?php

use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Filesystem\File;

$path = Configure::read('App.wwwRoot');

$file = new File($path . DS . 'css' . DS . 'pdf.css');
if($styles = $file->read()):

echo <<<STR
<style>
{$styles}
</style>

STR;

endif;

echo $this->fetch('content')
?>