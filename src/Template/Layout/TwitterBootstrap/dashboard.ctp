<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Controller\Component\AuthComponent;

$this->Html->css('BootstrapUI.dashboard', ['block' => true]);
$this->prepend('tb_body_attrs', ' class="' . implode(' ', array($this->request->controller, $this->request->action)) . '" ');
$this->start('tb_body_start');
?>
<body <?= $this->fetch('tb_body_attrs') ?>>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $this->Url->build('/') ?>"><?= Configure::read('App.title') ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right visible-xs contextual">
                    <?= $this->fetch('tb_actions') ?>
                </ul>
                <ul class="nav navbar-nav navbar-right visible-xs">
                    <li class="nav-divider"></li>
                    <?= $this->fetch('app_admin_menu') ?>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-divider"></li>
                    <?php if($this->has_update()): ?>
                    <li><a title="<?php echo __('New update available') ?>" href="<?= Router::url(['controller' => 'SelfUpdate']) ?>"><i class="glyphicon glyphicon-bell"></i></a></li>
                    <?php endif; ?>
                    <?php if($this->AuthUser->id()): ?>
                    <li><a href="<?= Router::url(['controller' => 'Users', 'action' => 'view', $this->AuthUser->id()]) ?>">@<?= $this->AuthUser->user('username') ?></a></li>
                    <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
                    <?php endif; ?>
                </ul><!--
                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Pesquisar...">
                </form> -->

            </div>
        </div>
    </div>

    <div class="container-fluid" id="content">
        <div class="row">
        <?php if($this->AuthUser->id()): ?>
            <div class="col-sm-3 col-md-2 sidebar">
            <?php if($actions = $this->fetch('tb_sidebar')): ?>
                <ul class="nav actions">
                    <li class="has-sub">
                        <h4 class="title"><?= __('Actions') ?></h4>
                        <?= $actions ?>
                    </li>
                </ul>
            <?php endif; ?>
                <?= $this->fetch('tb_admin_menu') ?>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <?php else: ?>
            <div class="col-md-12 main">
        <?php endif; ?>
<?php
/**
 * Default `flash` block.
 */
if (!$this->fetch('tb_flash')) {
    $this->start('tb_flash');
    if (isset($this->Flash))
        echo $this->Flash->render();
    $this->end();
}
$this->end();

$this->start('tb_body_end');
echo '</body>';
$this->end();

$this->append('content', '</div></div></div>');
echo $this->fetch('content');
