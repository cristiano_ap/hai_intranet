
<div style="display: none">
    <select id="products-base">
    <option value=""></option>
    <?php
        if(isset($products)) :
            foreach ($products as $product):
    ?>
    <option value="<?= $product->name ?>" product-id="<?= $product->id ?>" product-name="<?= $product->name ?>" product-sku="<?= $product->sku ?>" product-price="<?= $product->price ?>">
        <?= h($product->identifier) ?>
    <?php
            endforeach;
        endif;
    ?>
    </select>
</div>

<script>
(function($) {
    $(document).ready(function() {
        $('body').on('DOMSubtreeModified', function() {
            $('<?= $target ?>').each(function(i, el) {
                var did = $(el).data('did');
                if(!did) {
                    var target = $(el);
                    var attributes = target.prop('attributes');
                    var select = $('select#products-base').eq(0).clone().data('did', true);

                    $.each(attributes, function() {
                        select.attr(this.name, this.value);
                    });
                    target.replaceWith(select);

                    select
                        .chosen({
                            no_results_text: "=("
                        })
                        .on('change', function(e) {
                            var that = $(e.currentTarget);
                            var selected = $(e.currentTarget.selectedOptions[0]);
                            var sheet = $('#dynamic').calx('getSheet');

                            var elId = selected.parents('tr').find('.product-id');
                            var elPrice = selected.parents('tr').find('.product-price');

                            var cellId = $('#dynamic').calx('getCell', elId.attr('data-cell'));
                            var cellPrice = $('#dynamic').calx('getCell', elPrice.attr('data-cell'));

                            elId.val(selected.attr('product-id'));
                            cellId.setValue(selected.attr('product-id'));

                            cellPrice.setValue(selected.attr('product-price'));
                            elPrice.val(selected.attr('product-price'));

                            setTimeout(function() {
                                elId.trigger('change');
                                elPrice.trigger('change');
                                sheet.calculate();
                            }, 500);
                        });
                }
            });
        });
    });
})(jQuery);
</script>
