<fieldset>
    <legend><?= __('Receiver/Addressee') ?></legend>
    <?php
    echo $this->Form->input('client_id', [
        'options' => $clients,
        'empty' => true,
        'required' => true,
        'label' => __('Client'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_individual_registration', [
        'disabled' => true,
        'name' => '',
        'label' => __('Individual Registration'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_state_registration', [
        'disabled' => true,
        'label' => __('State Registration'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_phone1', [
        'disabled' => true,
        'label' => __('Phone1'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_zipcode', [
        'disabled' => true,
        'label' => __('Zipcode'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_address', [
        'disabled' => true,
        'label' => __('Address'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-8 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-8">{{content}}{{error}}</div>'
        ],
        ]);
    echo $this->Form->input('client_district', [
        'disabled' => true,
        'label' => __('District'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_city', [
        'disabled' => true,
        'label' => __('City'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ],
    ]);
    echo $this->Form->input('client_state', [
        'disabled' => true,
        'label' => __('State'),
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ],
    ]);
    ?>
</fieldset>
