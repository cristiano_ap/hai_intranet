<?php
$this->extend('../Layout/TwitterBootstrap/signin');
?>
<div class="users form">
<?php
$this->start('tb_flash');
$this->Flash->render('auth');
$this->end();
?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please enter your username and password') ?></legend>
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
    </fieldset>
<?= $this->Form->submit(__('Login')); ?>
<?= $this->Form->end() ?>
</div>
