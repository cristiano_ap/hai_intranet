<?php

use Cake\Core\Configure;

$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?php
    if(!$this->AuthUser->isMe($user->id)){
        $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $user->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
        );
    }
    ?>
    </li>
    <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>
<?= $this->Form->create($user); ?>
<fieldset>
    <legend><?= __('Edit {0}', [__('User')]) ?></legend>
    <?php
    echo $this->Form->input('username');
    echo $this->Form->input('password', ['type' => 'password','value' => '', 'after' => __('Keep empty to maintain current password'), 'required' => false]);
    if(!$this->AuthUser->isMe($user->id)){
        echo $this->Form->input('role', ['options' => array_flip(Configure::read('Roles'))]);
    }
    ?>
</fieldset>
<?= $this->Form->submit(__("Save")); ?>
<?= $this->Form->end() ?>
