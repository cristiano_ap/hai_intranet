<?php

use Cake\Core\Configure;

$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
<?= $this->Form->create($user); ?>
<fieldset>
    <legend><?= __('Add {0}', [__('User')]) ?></legend>
    <?php
    echo $this->Form->input('username');
    echo $this->Form->input('password');
    echo $this->Form->input('role', ['options' => array_flip(Configure::read('Roles'))]);
    ?>
</fieldset>
<?= $this->Form->submit(__("Add")); ?>
<?= $this->Form->end() ?>
