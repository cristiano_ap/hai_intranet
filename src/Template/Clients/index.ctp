<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Client'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('name'); ?></th>
            <th><?= $this->Paginator->sort('individual_registration'); ?></th>
            <th><?= $this->Paginator->sort('state_registration'); ?></th>
            <th class="hidden-md hidden-lg"><?= $this->Paginator->sort('address'); ?></th>
            <th><?= $this->Paginator->sort('city'); ?></th>
            <th class="hidden-md hidden-lg"><?= $this->Paginator->sort('district'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($clients as $client): ?>
        <tr>
            <td><?= h($client->id_formatted) ?></td>
            <td><?= h($client->name) ?></td>
            <td class="<?= $client->isCPF() ? 'cpf' : 'cnpj' ?>"><?= h($client->individual_registration_formatted) ?></td>
            <td><?= h($client->state_registration_formatted) ?></td>
            <td class="hidden-md hidden-lg"><?= h($client->address) ?></td>
            <td><?= h($client->city) ?></td>
            <td class="hidden-md hidden-lg"><?= h($client->district) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $client->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['controller' => 'Orders', 'action' => 'add', '?' => ['client_id' => $client->id]], ['title' => __('New Order'), 'class' => 'btn btn-default glyphicon glyphicon-list-alt']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $client->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
