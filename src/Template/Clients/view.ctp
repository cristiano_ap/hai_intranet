<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Client'), ['action' => 'edit', $client->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Client'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?> </li>
<li><?= $this->Html->link(__('New Client'), ['controller' => 'Clients', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add', '?' => ['client_id' => $client->id]]) ?> </li>
<li><?= $this->Html->link(__('New Quote'), ['controller' => 'Quotes', 'action' => 'add', '?' => ['client_id' => $client->id]]) ?> </li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($client->name) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Name') ?></td>
            <td><?= h($client->name) ?></td>
        </tr>
        <tr>
            <td><?= __('Individual Registration') ?></td>
            <td><?= h($client->individual_registration_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('State Registration') ?></td>
            <td><?= h($client->state_registration_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Address') ?></td>
            <td><?= h($client->address) ?></td>
        </tr>
        <tr>
            <td><?= __('City') ?></td>
            <td><?= h($client->city) ?></td>
        </tr>
        <tr>
            <td><?= __('District') ?></td>
            <td><?= h($client->district) ?></td>
        </tr>
        <tr>
            <td><?= __('Zipcode') ?></td>
            <td><?= h($client->zipcode_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Phone1') ?></td>
            <td><?= h($client->phone1) ?></td>
        </tr>
        <tr>
            <td><?= __('Phone2') ?></td>
            <td><?= h($client->phone2) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= h($client->id_formatted) ?></td>
        </tr>
        <tr>
            <td><?= __('Created') ?></td>
            <td><?= h($client->created) ?></td>
        </tr>
        <tr>
            <td><?= __('Modified') ?></td>
            <td><?= h($client->modified) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add', '?' => ['client_id' => $client->id]], ['style' => 'float: right']) ?>
        <h3 class="panel-title"><?= __('Related Orders') ?></h3>
    </div>
    <?php if (!empty($client->orders)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Final Price') ?></th>
                <th><?= __('Location') ?></th>
                <th><?= __('Exit Date') ?></th>
                <th><?= __('Paid') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($client->orders as $orders): ?>
                <tr>
                    <td><?= h($orders->id_formatted) ?></td>
                    <td><?= h($orders->final_price_formatted) ?></td>
                    <td><?= h($orders->location) ?></td>
                    <td><?= h($orders->exit_date_formatted) ?></td>
                    <td><?= h($orders->paid_formatted) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Orders', 'action' => 'view', $orders->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Orders</p>
    <?php endif; ?>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <?= $this->Html->link(__('New Quote'), ['controller' => 'Quotes', 'action' => 'add', '?' => ['client_id' => $client->id]], ['style' => 'float: right']) ?>
        <h3 class="panel-title"><?= __('Related Quotes') ?></h3>
    </div>
    <?php if (!empty($client->quotes)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Final Price') ?></th>
                <th><?= __('Location') ?></th>
                <th><?= __('Exit Date') ?></th>
                <th><?= __('Accepted') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($client->quotes as $quotes): ?>
                <tr>
                    <td><?= h($quotes->id_formatted) ?></td>
                    <td><?= h($quotes->final_price_formatted) ?></td>
                    <td><?= h($quotes->location) ?></td>
                    <td><?= h($quotes->exit_date_formatted) ?></td>
                    <td><?= h($quotes->accepted_formatted) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Quotes', 'action' => 'view', $quotes->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Quotes</p>
    <?php endif; ?>
</div>
