<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<?= $this->Form->create($client); ?>
<fieldset>
    <legend><?= __('Add {0}', [__('Client')]) ?></legend>
    <?php
    echo $this->Form->input('name', [
        'placeholder' => 'Google Ltda',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-12 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-12">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('individual_registration', [
        'placeholder' => '123.456.678-90/123.456.678/0001-00',
        'value' => $client->individual_registration,
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('state_registration', [
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('phone1', [
        'placeholder' => '99 12345-6789/99 1234-5678',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('phone2', [
        'placeholder' => '99 12345-6789/99 1234-5678',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-6 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-6">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('zipcode', [
        'placeholder' => '12345-678',
        'value' => $client->zipcode,
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('address', [
        'placeholder' => 'Av. ABC, 123',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-8 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-8">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('district', [
        'placeholder' => 'Centro',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('city', [
        'placeholder' => 'Contagem',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    echo $this->Form->input('state', [
        'placeholder' => 'MG',
        'templates' => [
            'inputContainer' => '<div class="form-group col-md-4 {{type}}{{required}}">{{content}}{{help}}</div>',
            'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error col-md-4">{{content}}{{error}}</div>'
        ]
    ]);
    ?>
</fieldset>
<?= $this->Form->submit(__("Add")); ?>
<?= $this->Form->end() ?>
