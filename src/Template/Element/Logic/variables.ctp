<?php
$this->start('app_variables');
?>
<script>
var App = {
	base: '<?= $this->request->base ?>',
	url: '<?= $this->request->url ?>',
	webroot: '<?= $this->request->webroot ?>',
	here: '<?= $this->request->here ?>'
}
</script>
<?php
$this->end();
$this->append('meta', $this->fetch('app_variables'));