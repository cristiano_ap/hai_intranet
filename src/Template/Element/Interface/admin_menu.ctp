<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('app_admin_menu');

?>
<li><?= $this->Html->link(__('Clients'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
<li><?= $this->Html->link(__('Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('Quotes'), ['controller' => 'Quotes', 'action' => 'index']) ?> </li>

<?php
$this->end();
$this->append('tb_admin_menu', '<ul class="nav nav-sidebar admin-menu">' . $this->fetch('app_admin_menu') . '</ul>');
