<?php
use Cake\Filesystem\File;
use Cake\Core\Configure;

$this->start('app_scripts');
echo $this->Html->script('/bower_components/jquery/dist/jquery.min');

//  echo $this->Html->css('/bower_components/bootstrap/dist/css/bootstrap-theme.min');
echo $this->Html->css('/bower_components/bootstrap/dist/css/bootstrap.min');
echo $this->Html->script('/bower_components/bootstrap/dist/js/bootstrap.min');

echo $this->Html->css('/bower_components/footable/css/footable.core.min');
echo $this->Html->script('/bower_components/footable/dist/footable.min');

echo $this->Html->script('/bower_components/jquery-calx/js/numeral.min');
echo $this->Html->script('/bower_components/jquery-calx/jquery-calx-2.2.7.min');

echo $this->Html->css('/bower_components/chosen/chosen.min');
echo $this->Html->css('/bower_components/chosen-bootstrap/chosen.bootstrap.min');
echo $this->Html->script('/bower_components/chosen/chosen.jquery.min');

echo $this->Html->css('/bower_components/ladda-bootstrap/dist/ladda-themeless.min');
echo $this->Html->script('/bower_components/ladda-bootstrap/dist/spin.min');
echo $this->Html->script('/bower_components/ladda-bootstrap/dist/ladda.min');

echo $this->Html->script('/bower_components/matchHeight/dist/jquery.matchHeight-min');
echo $this->Html->script('/bower_components/enquire/dist/enquire.min');

echo $this->Html->script('/bower_components/jquery-mask-plugin/dist/jquery.mask.min');

echo $this->Html->script('utils');
echo $this->Html->script('forms');
echo $this->Html->script('tables');
echo $this->Html->script('zipcode');
echo $this->Html->script('behaviors');


$c = $this->request->controller;
$a = $this->request->action;

$js_scoped = WWW_ROOT . Configure::read('App.jsBaseUrl') . $c . DS . $a . ".js";
$css_scoped = WWW_ROOT . Configure::read('App.cssBaseUrl') . $c . DS . $a . ".css";
$js_controller_scoped = WWW_ROOT . Configure::read('App.jsBaseUrl') . $c . DS . "all.js";
$css_controller_scoped = WWW_ROOT . Configure::read('App.cssBaseUrl') . $c . DS . "all.css";

if((new File($js_scoped))->exists()){
	echo $this->Html->script("$c/$a" . '.js');
}
if((new File($css_scoped))->exists()){
	echo $this->Html->css("$c/$a" . '.css');
}
if((new File($js_controller_scoped))->exists()){
	echo $this->Html->script("$c/all.js");
}
if((new File($css_controller_scoped))->exists()){
	echo $this->Html->css("$c/all.css");
}


$this->end();
$this->prepend('css', $this->fetch('app_scripts'));
