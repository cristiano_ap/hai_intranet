<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Network\Exception\MethodNotAllowedException;

/**
 * Quotes Controller
 *
 * @property \App\Model\Table\QuotesTable $Quotes
 */
class QuotesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients'],
            'quote' => [
                'Quotes.id' => 'desc'
            ]
        ];
        $this->set('quotes', $this->paginate($this->Quotes));
        $this->set('_serialize', ['quotes']);
    }

    /**
     * View method
     *
     * @param string|null $id Quote id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $quote = $this->Quotes->get($id, [
            'contain' => ['Clients']
        ]);
        $this->set('quote', $quote);
        $this->set('_serialize', ['quote']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $quote = $this->Quotes->newEntity();
        if ($this->request->is('post')) {
            $quote = $this->Quotes->patchEntity($quote, $this->request->data);
            if ($this->Quotes->save($quote)) {
                $this->Flash->success(__('The quote has been saved.'));
                $this->Quotes->generatePdf($quote);
                return $this->redirect(['action' => 'view', $quote->id]);
            } else {
                $this->Flash->error(__('The quote could not be saved. Please, try again.'));
            }
        }
        $clients = $this->Quotes->Clients->find('list', ['limit' => 200, 'valueField' => 'identifier']);
        $this->set(compact('quote', 'clients'));
        $this->set('_serialize', ['quote']);
    }

    /**
     * Generate PDF method
     *
     * @return void Redirects on successful generation, renders view otherwise.
     */
    public function generatePdf($id = null)
    {
        // $this->request->allowMethod(['patch', 'post', 'put']);

        $quote = $this->Quotes->get($id, [
            'contain' => []
        ]);
        if ($this->Quotes->generatePdf($quote)) {
            $this->Flash->success(__('The PDF quote has been generated.'));
        } else {
            $this->Flash->error(__('The PDF quote could not be generated. Please, try again.'));
        }
        return $this->redirect(['action' => 'view', $quote->id]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Quote id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Nobody can edit
        throw new MethodNotAllowedException(__('This action is not allowed'));

        //But nothing remains forever
        $quote = $this->Quotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $quote = $this->Quotes->patchEntity($quote, $this->request->data);
            if ($this->Quotes->save($quote)) {
                $this->Flash->success(__('The quote has been saved.'));
                return $this->redirect(['action' => 'view', $quote->id]);
            } else {
                $this->Flash->error(__('The quote could not be saved. Please, try again.'));
            }
        }
        $clients = $this->Quotes->Clients->find('list', ['limit' => 200]);
        $this->set(compact('quote', 'clients'));
        $this->set('_serialize', ['quote']);
    }

    /**
     * Set as accepted method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Network\Response|null Redirects to view.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function setAsAccepted($id = null)
    {
        $quote = $this->Quotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $quote = $this->Quotes->get($id);
            if ($this->Quotes->setAsAccepted($quote)) {
                $this->Flash->success(__('The quote has been saved.'));
                return $this->redirect(['action' => 'view', $id]);
            } else {
                $this->Flash->error(__('The quote could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['action' => 'view', $quote->id]);
    }

    /**
     * Generate order method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Network\Response|null Redirects to Order::add.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function generateOrder($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $quote = $this->Quotes->get($id);
            if ($this->Quotes->setAsAccepted($quote)) {
                return $this->redirect(['controller' => 'Orders', 'action' => 'add', '?' => ['from_quote' => $id]]);
            } else {
                $this->Flash->error(__('The order could not be generated. Please, set the quote as accepted first.'));
            }
        }
        return $this->redirect(['action' => 'view', $quote->id]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $quote = $this->Quotes->get($id);
        if ($this->Quotes->delete($quote)) {
            $this->Flash->success(__('The quote has been deleted.'));
        } else {
            $this->Flash->error(__('The quote could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
