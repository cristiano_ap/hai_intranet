<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Network\Exception\MethodNotAllowedException;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients'],
            'order' => [
                'Orders.id' => 'desc'
            ]
        ];
        $this->set('orders', $this->paginate($this->Orders));
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Clients']
        ]);
        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                $this->Orders->generatePdf($order);
                return $this->redirect(['action' => 'view', $order->id]);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        } elseif(!$this->request->is('post') && isset($this->request->query['from_quote'])){
            $quote_id = $this->request->query['from_quote'];
            $this->request->data = $this->Orders->getFromQuote($quote_id);
            $order = $this->Orders->patchEntity($order, $this->request->data);
        }
        $clients = $this->Orders->Clients->find('list', ['limit' => 200, 'valueField' => 'identifier']);
        $this->set(compact('order', 'clients'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Generate PDF method
     *
     * @return void Redirects on successful generation, renders view otherwise.
     */
    public function generatePdf($id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);

        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->Orders->generatePdf($order)) {
            $this->Flash->success(__('The PDF order has been generated.'));
        } else {
            $this->Flash->error(__('The PDF order could not be generated. Please, try again.'));
        }
        return $this->redirect(['action' => 'view', $order->id]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //Nobody can edit
        throw new MethodNotAllowedException(__('This action is not allowed'));

        //But nothing remains forever
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'view', $order->id]);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $this->set(compact('order', 'clients'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Set as paid method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to view.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function setAsPaid($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->get($id);
            if ($this->Orders->setAsPaid($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'view', $id]);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['action' => 'view', $order->id]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
