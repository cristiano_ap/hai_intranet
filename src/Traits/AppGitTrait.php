<?php

namespace App\Traits;

use Cake\Network\Http\Client;
use Cake\Core\Configure;
use Cake\Cache\Cache;

/**
 * AppGitTrait
 *
 * Utility to check AppGit info
 */
trait AppGitTrait
{

    protected function _get_last_commit()
    {
        try
        {
            $http = new Client;
            $base_url = Configure::read('Git.url');
            $response = $http->get($base_url . '/commits/master');

            return $response->json['values'][0];
        } catch (\Exception $e)
        {
            return '';
        }
    }

    public final function has_update()
    {
        return Cache::remember('app-has-update', function() {
            $last_commit = substr($this->_get_last_commit()['hash'], 0, 12);
            $current_hash = explode(' ', file_get_contents(ROOT . DS . 'version'))[0];

            $has_update = $last_commit !== $current_hash;
            Cache::write('app-has-update', $has_update);
            return $has_update;
        });
    }
}
