<?php

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;

class HaiShell extends Shell
{
    public $tasks = ['Update'];

    public function main()
    {
        $this->out('<info>Hai Tecnologia</info>');
        $this->hr();
        $this->out('[C]heck updates');
        $this->out('[I]nstall update');
        $this->out('[Q]uit');

        $choice = strtolower($this->in('What would you like to do?', ['C', 'I', 'Q']));
        switch ($choice)
        {
            case 'c':
                $this->Update->main('check');
                break;
            case 'i':
                $this->Update->main('install');
                break;
            case 'q':
                $this->_stop();

                return;
            default:
                $this->out('You have made an invalid selection. Please choose a command to execute by entering C, I, or Q.');
        }
        $this->hr();
        $this->main();

        // https://book.cakephp.org/3.0/pt/console-and-shells.html
    }

    public function update($action = null)
    {
        $this->Update->main($action);
    }
}
