<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use \PhpZip\ZipFile;
use App\Traits\AppGitTrait;

/**
 * Task for unloading plugins.
 */
class UpdateTask extends Shell
{
    use AppGitTrait;

    const UPDATE_FOLDER = TMP . DS . 'update';
    const CURRENT_FOLDER = TMP . DS . 'current';

    /**
     * Execution method always used for tasks.
     *
     * @param string|null $plugin The plugin name.
     * @return bool if action passed.
     */
    public function main($action = null)
    {

        $this->out('Hai updater tool');
        $this->hr();

        switch ($action) {
            case 'check':
                $this->updateCheck();
                break;
            case 'install':
                $this->updateInstall();
                break;
            case 'revert':
                $this->revertUpdate();
                break;
        }
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->description(
            'Updater'
        )
        ->addArgument('action', [
            'help' => 'Check for updates or install directly',
            'required' => true,
            'choices' => ['check', 'install']
        ]);

        return $parser;
    }

    protected function updateCheck()
    {
        try
        {
            $progress = $this->helper('progress');
            $progress->init(['total' => 2]);
            $progress->increment(1);
            $progress->draw();

            $last_commit = $this->_get_last_commit();
            $current_hash = file_get_contents(ROOT . DS . 'version');

            $progress->increment(1);
            $progress->draw();
            $this->out($this->nl(1));

            if($this->has_update())
            {
                $this->info('There is a new version!');
                $this->info('Run cake hai update install');
            } else {
                $this->out('You are running the last version');
            }
        } catch (\Exception $e)
        {
            $this->err('Is not posible to check for updates');
        }

        return;
    }

    protected function updateInstall()
    {
        try
        {
            $maintenance = new File(TMP . DS . 'maintenance', true);
            register_shutdown_function(function() use($maintenance) {
                $maintenance->delete();
            });

            $progress = $this->helper('progress');
            $progress->init(['total' => 7]);

            $progress->increment(1);
            $progress->draw();

            $root = new Folder(ROOT);
            $root->copy([
                'to' => self::CURRENT_FOLDER,
                'skip' => ['vendor', '.git', 'node_modules', '.sass-cache', 'logs', 'tmp'],
            ]);
            if($errors = $root->errors())
            {
                $this->err($errors);
                $this->abort();
            }

            $progress->increment(1);
            $progress->draw();
            $this->info(' Backup successful');

            $update = new Folder(self::UPDATE_FOLDER);
            $update->delete();

            $progress->increment(1);
            $progress->draw();
            $this->info(' Temp folder set');

            $http = new Client;
            $response = $http->get(Configure::read('Git.zip_url'));

            $progress->increment(1);
            $progress->draw();
            $this->info(' Downloaded last version');

            $git_zip = new File(self::UPDATE_FOLDER . DS . 'hai.zip', true);
            $git_zip->append($response->body);

            $progress->increment(1);
            $progress->draw();
            $this->info(' File saved on disc');

            $zip = new ZipFile;
            $zip->openFile($git_zip->path)
                ->extractTo(self::UPDATE_FOLDER)
                ->close();

            $git_zip->delete();

            $progress->increment(1);
            $progress->draw();
            $this->out(' Ready to update');

            $last_commit = $this->_get_last_commit();

            // $extract_folder = $update->read()[0][0];
            $extract_folder = 'cristiano_ap-hai_intranet-' . substr($last_commit['hash'], 0, 12);
            $extracted = new Folder(self::UPDATE_FOLDER . DS . $extract_folder);
            $extracted->move(ROOT);

            $string = substr($last_commit['hash'], 0, 12) . ' ' . $last_commit['summary']['raw'];
            file_put_contents(ROOT . DS . 'version', $string);

            $progress->increment(1);
            $progress->draw();
            $this->info(' Stored last version info');

            $this->success('Done, running migrations');

            $this->dispatchShell('migrations migrate');
            $this->info('Installing dependencies');

            system('bower install -f');
            system('composer install -n');

            $this->success('Finish!');
        }
        catch (\Exception $e)
        {
            $this->error(' Update failed: ' . $e->getMessage());
        }
    }

    public function revertUpdate()
    {
        try
        {
            // $folder = new Folder(self::UPDATE_FOLDER);
            // $folder->move(ROOT);

            // $this->out('Rolled back update');
            // $this->dispatchShell('migrations migrate');
        } catch (\Exception $e)
        {
            $this->abort('Could not rollback application');
        }
    }
}
