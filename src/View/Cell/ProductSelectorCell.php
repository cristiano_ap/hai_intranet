<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * UserSearch cell
 */
class ProductSelectorCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($target)
    {
        $this->loadModel('Products');
        $products = $this->Products->find();
        $this->set(compact(['products', 'target']));
    }
}
