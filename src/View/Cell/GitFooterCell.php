<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Core\Configure;

/**
 * UserSearch cell
 */
class GitFooterCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $footer = '';
        try {
            $footer = @file_get_contents(ROOT . DS . 'version');
        } catch (\Exception $e) {
            $footer = $e->getMessage();
        }

        $this->set(compact(['footer']));
    }
}
