<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * UserSearch cell
 */
class UserSearchCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $this->loadModel('Clients');
        $clients = $this->Clients->find('list', ['limit' => 200, 'valueField' => 'identifier']);
        $this->set('clients', $clients);
    }
}
