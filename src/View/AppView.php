<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use Cake\View\View;
use Cake\Core\Plugin;
use App\Traits\AppGitTrait;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends \CrudView\View\CrudView
{
    use AppGitTrait;

    // public $layout = 'BootstrapUI.default';
    public $layout = 'backend';

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

    }

    /**
     * Setup helpers
     *
     * @return void
     */
    protected function _setupHelpers()
    {
        $this->loadHelper('Burzum/UserTools.Auth');
        $this->loadHelper('Tools.AuthUser');
        $this->loadHelper('Html', ['className' => 'BootstrapUI.Html']);
        $this->loadHelper('Flash', ['className' => 'BootstrapUI.Flash']);
        $this->loadHelper('Paginator', ['className' => 'BootstrapUI.Paginator']);
        $this->loadHelper('Form', [
            'className' => 'BootstrapUI.Form',
            'templates' => [
                'inputContainer' => '<div class="form-group col-md-12 {{type}}{{required}}">{{content}}{{help}}</div>',
                'dateWidget' => '<ul class="list-inline"><li class="day">{{day}}</li><li class="month">{{month}}</li><li class="year">{{year}}</li><li class="hour">{{hour}}</li><li class="minute">{{minute}}</li><li class="second">{{second}}</li><li class="meridian">{{meridian}}</li></ul>',
                'checkboxWrapper' => '<div class="checkbox col-md-12">{{label}}</div>',
                'submitContainer' => '<div class="submit col-md-12">{{content}}</div>',
                'error' => '<div class="error-message col-md-12">{{content}}</div>',
                'inputContainerError' => '<div class="input {{type}}{{required}} has-error col-md-12">{{content}}{{error}}</div>',
            ]
        ]);

        $this->loadHelper('CrudView.CrudView');

        if (Plugin::loaded('AssetCompress')) {
            $this->loadHelper('AssetCompress.AssetCompress');
        }
    }



    /**
     * Read from config which css and js files to load, and add them to the output.
     * If `AssetCompress` plugin is loaded, use the `asset_compress.ini` configuration
     * that is part of this plugin.
     *
     * @return void
     */
    protected function _loadAssets()
    {
    }
}
