'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        'recursive-compass': {
            dev: {
                src: ['webroot/scss/**/*.{scss,sass}'],
                options: {
                    outputStyle: 'expanded',
                    sassDir: 'webroot/scss',
                    cssDir: 'webroot/css'
                }
            },
            build: {
                src: ['webroot/scss/**/*.{scss,sass}'],
                options: {
                    outputStyle: 'compressed',
                    sassDir: 'webroot/scss',
                    cssDir: 'webroot/css'
                }
            }
        },
        watch: {
            dev: {
                files: ['webroot/scss/**/*.{scss,sass}'],
                tasks: ['recursive-compass:dev']
            }
        }
    });
    // Load the module
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-recursive-compass');
    // Default task.
    grunt.registerTask('default', 'recursive-compass:build');
};