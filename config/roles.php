<?php 

/**
* Optionally define constants for easy referencing throughout your code
*/
define('ROLE_USER', 20);
define('ROLE_ADMIN', 80);
define('ROLE_SUPERADMIN', 99);

return [
    'Roles' => [
        'user' => ROLE_USER,
        'admin' => ROLE_ADMIN,
        'superadmin' => ROLE_SUPERADMIN
    ]
];