<?php
use Migrations\AbstractMigration;

class AddDiscountToOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('orders');
        $table->addColumn('discount', 'float', [
            'default' => null,
            'null' => false,
            'precision' => 5,
            'scale' => 2,
            'after' => 'client_id'
        ]);
        $table->addColumn('amount_advanced', 'float', [
            'default' => null,
            'null' => false,
            'precision' => 10,
            'scale' => 2,
            'after' => 'discount'
        ]);
        $table->update();
    }
}
