<?php
use Migrations\AbstractMigration;

class AddOrderIdToQuotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('quotes');
        $table
            ->addColumn('order_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'deleted'
            ])
            ->renameColumn('paid', 'accepted');
        $table->update();

        $table = $this->table('orders');
        $table
            ->addColumn('quote_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'deleted'
            ]);
        $table->update();
    }
}
