<?php
use Migrations\AbstractMigration;

class RemoveDiscountFromOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('orders');
        $table->removeColumn('discount');
        $table->removeColumn('amount_advanced');
        $table->update();
    }
}
